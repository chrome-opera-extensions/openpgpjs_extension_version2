(function(_w, _d, _c) {
     'use strict';

     var popupProcesses = {
          showNotification(data, cost) {
               _c.popupProcesses.notification = new Notification(data.title, {
                    icon: '../images/orange128.png',
                    body: data.body
               });
               if(cost) {
                    setTimeout(_c.popupProcesses.notification.close.bind(_c.popupProcesses.notification), cost);
               }
		},
		closeNotification() {
			if(_c.popupProcesses.notification) {
				setTimeout(_c.popupProcesses.notification.close.bind(_c.popupProcesses.notification), 0);
			}
		},
          showBookmarks: () => {
               var openbookmarks = _d.getElementById("openbookmarks");
               openbookmarks.addEventListener('click', () => {
                    _c.tabs.create({
                         url: "/html/bookmarks.html"
                    });
               });
          },
          addExportKeys: () => {
               _d.getElementById("exportkeys").addEventListener("click", () => {
                    _c.p.storage.local.get("opgpbms").then(function(results) {
                         var o = results;
                         var privkey = atob(o.opgpbms.privateKey);
                         var allLines = privkey.split(/\r\n|\n/);
                         var lines = null;
                         allLines.map((line) => {
                              lines = (lines === null) ? line + '\r\n' : lines + line + '\r\n';
                         });
                         privkey = lines.trim();
                         var pubkey = atob(o.opgpbms.publicKey);
                         allLines = pubkey.split(/\r\n|\n/);
                         lines = null;
                         allLines.map((line) => {
                              lines = (lines === null) ? line + '\r\n' : lines + line + '\r\n';
                         });
                         pubkey = lines.trim();
                         var combinedKeys = pubkey + '\r\n\r\n' + privkey;
                         _c.tabs.query({
                              active: true,
                              currentWindow: true
                         }, (tabs) => {
                              _c.tabs.sendMessage(tabs[0].id, {
                                   sendkeys: combinedKeys
                              }, (response) => {
                                   //window.console.log(response.gotkeys);
                              });
                         });
                    });
               });
          },
          addExportBookmarks: () => {
               _d.getElementById("exportbmks").addEventListener("click", () => {
                    _c.p.storage.local.get("opgpbms").then(function(results) {
                         var o = results;
                         var crt = atob(o.opgpbms.db);
                         _c.tabs.query({
                              active: true,
                              currentWindow: true
                         }, (tabs) => {
                              _c.tabs.sendMessage(tabs[0].id, {
                                   sendBookmarks: crt
                              }, (response) => {
                                   //window.console.log(response.gotBookmarks);
                              });
                         });
                    });
               });
          },
          testTags(tags) {
               var IsCorrect = true;
               if(tags === null) {
                    IsCorrect = false;
               }
               if(/,\s./.test(tags)) {
                    tags = tags.split(", ").join(",");
                    if(/\s/.test(tags)) {
                         IsCorrect = false;
                    }
               }
               if(/,[^\s]./.test(tags)) {
                    if(/\s/.test(tags)) {
                         IsCorrect = false;
                    }
               }
               if(!(/,/.test(tags)) && (/\s/.test(tags))) {
                    IsCorrect = false;
               }
               return IsCorrect;
          },
          addOpenpgpBookMark: () => {
               _d.getElementById("addbookmark").addEventListener("click", () => {
                    _c.tabs.query({
                         "active": true,
                         "lastFocusedWindow": true
                    }, (tabs) => {
                         if(/(chrome|opera|chrome-extension):/.test(tabs[0].url)) {
                              var newalert = `Protocol: ${/.*:\/\//.exec(tabs[0].url)}\n` +
                                   "Bookmarking this protocol is not allowed.";
                              _c.popupProcesses.showNotification({
                                   title: "Bookmark Error",
                                   body: newalert
                              }, 5000);
                              //var newalert = `Protocol: ${/.*:\/\//.exec(tabs[0].url)}\n\n` +
                              //     `Bookmarking of the following protocols is not allowed:\n` +
                              //     `     _c://\n` +
                              //     `     opera://\n` +
                              //     `     _c-extension://\n`
                              //_c.tabs.sendMessage(tabs[0].id, {
                              //     sendAlert: {
                              //          'header': 'Bookmark Error',
                              //          'text': `${newalert}`,
                              //          'type': 'error'
                              //     }
                              //}, (response) => {
                              //     //window.console.log(response.gotAlert);
                              //});
                              return;
                         } else {
                              var cssText = `html,body {
							width: 350px;
							height: 95px;
						}`;
                              var style = _d.createElement("style");
                              style.id = "openpgppopup";
                              var cssTextNode = _d.createTextNode(cssText);
                              style.appendChild(cssTextNode);
                              _d.head.appendChild(style);
                              _d.getElementById("container").style.display = "none";
                              _d.getElementById("bkmkwindow").style.display = "block";
                              _d.getElementById("bkmk").value = tabs[0].title;
                              _d.getElementById("tags").value = "#hashtags";
                              _d.getElementById("bkmkbtn").addEventListener("click", () => {
							_c.popupProcesses.closeNotification();
							_c.popupProcesses.showNotification({
								title: "Adding Bookmark...",
								body: "Encrypting new bookmark."
							});
                                   var response = _d.getElementById("bkmk").value;
                                   _d.getElementById("openpgppopup").remove();
                                   _d.getElementById("container").style.display = "block";
                                   _d.getElementById("bkmkwindow").style.display = "none";
                                   _c.cryptog.getDatabase().then(function(db) {
                                        db = JSON.parse(db);
                                        var tagResponse = _d.getElementById("tags").value;
                                        var tagIsCorrect = _c.popupProcesses.testTags(tagResponse);
                                        if(!tagIsCorrect) {
                                             tagResponse = prompt('Set hashtag\nThe #hashtags you entered had space(s).\nTry again.', '#hashtag');
                                        }
                                        tagIsCorrect = _c.popupProcesses.testTags(tagResponse);
                                        if(!tagIsCorrect) {
                                             tagResponse = null;
                                        }
                                        tagResponse = (/#/.test(tagResponse)) ? tagResponse.replace(/#/g, '') : tagResponse;
                                        tagResponse = (/,/.test(tagResponse)) ? tagResponse.split(',') : [tagResponse];
                                        var tags = [];
                                        tagResponse.forEach(function(item) {
                                             if(item !== null && item !== "") {
                                                  item = item.trim();
                                                  tags.push(item);
                                             }
                                        });
                                        var bookmarkResponse = {
                                             title: response,
                                             tags: tags
                                        }
                                        var bookmark = {
                                             id: db[db.length - 1].id + 1,
                                             title: JSON.stringify(bookmarkResponse),
                                             url: tabs[0].url
                                        }
                                        db = db.concat(bookmark);
                                        _c.cryptog.encryptItem(JSON.stringify(db)).then(function(e) {
                                             _c.p.storage.local.get("opgpbms").then(function(results) {
                                                  var o = results;
                                                  o.opgpbms.db = btoa(e);
                                                  _c.p.storage.local.set(o).then(function(bool) {
                                                  //if(bool) {
                                                  //     _c.popupProcesses.showNotification({
                                                  //          title: "Bookmark Added",
                                                  //          body: `Title:\n${response}`
                                                  //     }, 10000);
                                                  //     //_c.tabs.sendMessage(tabs[0].id, {
                                                  //     //     sendAlert: {
                                                  //     //          header: 'Bookmark Added',
                                                  //     //          text: `Title:\n${response}`,
                                                  //     //          type: 'success'
                                                  //     //     }
                                                  //     //});
										//}
											_c.popupProcesses.closeNotification();
                                                  });
                                             });
                                        })
                                   });
                              });
                         }
                    });
               });
          },
          syncBookmarks: () => {
               var syncbmks = _d.getElementById("syncbmks");
               syncbmks.addEventListener("click", () => {
                    _c.tabs.create({
                         url: "/html/sync.html"
                    });
               });
          },
          getLoginTime: () => {
               return new Promise((resolve) => {
                    _c.p.storage.local.get("opgpbms").then(function(results) {
                         resolve(results.opgpbms.LoginTime);
                    });
               });
          },
          checkSession: () => {
               return new Promise((resolve) => {
                    _c.popupProcesses.getLoginTime().then((LoginTime) => {
                         let loginTime = new Date(LoginTime);
                         let now = new Date();
                         var diff = +now - +loginTime;
                         var TotalMilliseconds = diff;
                         var TotalSeconds = Math.round(TotalMilliseconds / 1000);
                         var TotalMinutes, TotalTime;
                         if(isNaN(TotalSeconds)) {
                              TotalTime = NaN;
                              console.log("checkSession:", "session is closed");
                         } else if((TotalSeconds / 60) >= 1) {
                              TotalMinutes = Math.round(TotalSeconds / 60);
                              var Seconds = parseInt(TotalSeconds % 60);
                              TotalTime = `${TotalMinutes} min, ${Seconds} sec`;
                              console.log("checkSession:", "Time logged in =", TotalTime);
                         } else {
                              TotalMinutes = parseInt("");
                              TotalTime = TotalSeconds + " seconds";
                              console.log("checkSession:", "Time logged in =", TotalTime);
                         }
                         if(diff < 600000) {
                              resolve({
                                   open: true,
                                   totalTime: TotalTime
                              });
                         } else {
                              resolve({
                                   open: false,
                                   totalTime: TotalTime
                              });
                         }
                    });
               });
          },
          checkLogin: () => {
               _c.popupProcesses.checkSession().then((results) => {
                    if(results.open) {
                         if(_d.getElementById("addbookmark").disabled) {
                              _d.getElementById("addbookmark").disabled = false;
                         }
                         if(_d.getElementById("exportkeys").disabled) {
                              _d.getElementById("exportkeys").disabled = false;
                         }
                         if(_d.getElementById("exportbmks").disabled) {
                              _d.getElementById("exportbmks").disabled = false;
                         }
                         if(_d.getElementById("syncbmks").disabled) {
                              _d.getElementById("syncbmks").disabled = false;
                         }
                         _d.getElementById("logintime").innerHTML = "Last login:<br>" + results.totalTime;
                    }
               });
          },
          buildPopup: function() {
               _c.popupProcesses.checkLogin();
               _c.popupProcesses.addOpenpgpBookMark();
               _c.popupProcesses.showBookmarks();
               _c.popupProcesses.addExportKeys();
               _c.popupProcesses.addExportBookmarks();
               _c.popupProcesses.syncBookmarks();
               _d.removeEventListener("DOMContentLoaded", _c.popupProcesses.buildPopup);
          }
     }
     _c.popupProcesses = popupProcesses;
})(window, document, chrome);

document.addEventListener("DOMContentLoaded", chrome.popupProcesses.buildPopup);
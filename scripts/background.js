(function(_w, _d, _c) {
     'use strict';

     _c.backgroundProcs = {
          login() {
               _c.tabs.create({
                    url: "/html/bookmarks.html"
               });
          },
          promptuser() {
               _c.storage.local.get('opgpbms', function(verify) {
                    var o = verify;
                    if(o.opgpbms.publicKey !== null) {
                         _c.backgroundProcs.addBookmark();
                    } else {
                         _c.tabs.create({
                              url: "/html/bookmarks.html"
                         });
                    }
               });
          },
          clearOBSession() {
               _c.storage.local.get('opgpbms', function(results) {
                    var o = results;
                    o.opgpbms.LoginTime = null;
                    o.opgpbms.bkmkpage = null;
                    o.opgpbms.tgp = null;
                    _c.p.storage.local.set(o);
                    createContextMenus();
               });
          },
          clearOBSessionWReload() {
               if(MENUBOOKMARKIT) {
                    _c.contextMenus.remove(MENUBOOKMARKIT);
                    MENUBOOKMARKIT = null;
               }
               if(MENUOPENBOOKMARKS) {
                    _c.contextMenus.remove(MENUOPENBOOKMARKS);
                    MENUOPENBOOKMARKS = null;
               }
               if(MENUSEPERATOR) {
                    _c.contextMenus.remove(MENUSEPERATOR);
                    MENUSEPERATOR = null;
               }
               if(MENULOGOUT) {
                    _c.contextMenus.remove(MENULOGOUT);
                    MENULOGOUT = null;
               }
               if(MENULOGIN) {
                    _c.contextMenus.remove(MENULOGIN);
                    MENULOGIN = null;
               }
               _c.backgroundProcs.clearOBSession();
               /*
               chrome.tabs.getSelected(null, function(tab) {
               	var code = 'window.location.reload();';
               	chrome.tabs.executeScript(tab.id, { code: code });
               });
               */
          },
          testTags(tags) {
               var IsCorrect = true;
               if(tags === null) {
                    IsCorrect = false;
               }
               if(/,\s./.test(tags)) {
                    tags = tags.split(", ").join(",");
                    if(/\s/.test(tags)) {
                         IsCorrect = false;
                    }
               }
               if(/,[^\s]./.test(tags)) {
                    if(/\s/.test(tags)) {
                         IsCorrect = false;
                    }
               }
               if(!(/,/.test(tags)) && (/\s/.test(tags))) {
                    IsCorrect = false;
               }
               return IsCorrect;
          },
          showNotification(data, cost) {
               _c.backgroundProcs.notification = new Notification(data.title, {
                    icon: 'images/orange128.png',
                    body: data.body
               });
               if(cost) {
                    setTimeout(_c.backgroundProcs.notification.close.bind(_c.backgroundProcs.notification), cost);
               }
          },
          addBookmark() {
               _c.tabs.query({
                    "active": true,
                    "lastFocusedWindow": true
               }, function(tabs) {
                    //alert(tabs[0].url);
                    if(/(chrome|opera|chrome-extension):/.test(tabs[0].url)) {
                         var newalert = `Protocol: ${/.*:\/\//.exec(tabs[0].url)}\n` +
                              "Bookmarking this protocol is not allowed.";
                         _c.backgroundProcs.showNotification({
                              title: "Bookark Error",
                              body: newalert
                         }, 2000);
                         //_c.tabs.sendMessage(tabs[0].id, {
                         //     sendAlert: {
                         //          'header': 'Bookmark Error',
                         //          'text': newalert,
                         //          'type': 'error'
                         //     }
                         //}, function(response) {
                         //     //console.log(response.gotAlert);
                         //});
                         //console.log(tabs[0].url);
                         return;
                    }
                    _c.storage.local.get('opgpbms', function(results) {
                         var o = results;
                         var failed = false;
                         var bookmarkResponse = prompt("Set Bookmark Title", tabs[0].title);
                         if(!bookmarkResponse) {
                              alert("Bookmark canceled!");
                              return;
                         }
                         console.log(bookmarkResponse);
                         var tagResponse = prompt('Set hashtag\nEnter #hashtags seperated by commas with no space.', '#hashtag');
                         var tagIsCorrect = _c.backgroundProcs.testTags(tagResponse);
                         if(!tagIsCorrect) {
                              tagResponse = prompt('Set hashtag\nThe #hashtags you entered had space(s).\nTry again.', '#hashtag');
                         }
                         tagIsCorrect = _c.backgroundProcs.testTags(tagResponse);
                         if(!tagIsCorrect) {
                              tagResponse = null;
                         }
                         tagResponse = (/#/.test(tagResponse)) ? tagResponse.replace(/#/g, '') : tagResponse;
                         tagResponse = (/,/.test(tagResponse)) ? tagResponse.split(',') : [tagResponse];
                         var tags = [];
                         tagResponse.forEach(function(item) {
                              if(item !== null && item !== "") {
                                   item = item.trim();
                                   tags.push(item);
                              }
                         });
                         bookmarkResponse = {
                              title: bookmarkResponse,
                              tags: tags
                         }
                         console.log(bookmarkResponse);
                         if(bookmarkResponse.title) {
                              if(o.opgpbms.db.length !== 0) {
                                   _c.cryptog.decryptItem(atob(o.opgpbms.db)).then(function(db) {
                                        //_c.backgroundProcs.dItem
                                        var database = JSON.parse(db);
                                        console.log(database);
                                        var EBMID = database.length + 1;
                                        var items = {
                                             id: EBMID,
                                             title: JSON.stringify(bookmarkResponse),
                                             url: tabs[0].url
                                        }
                                        database.push(items);
                                        //console.log(database);
                                        _c.cryptog.encryptItem(JSON.stringify(database)).then(function(etext) {
                                             //_c.backgroundProcs.eItem(JSON.stringify(database)).then(function(etext) {
                                             o.opgpbms.db = btoa(etext);
                                             _c.storage.local.set(o, function() {
                                                  _c.tabs.sendMessage(tabs[0].id, {
                                                       sendAlert: {
                                                            'header': 'Bookmark Added',
                                                            'text': `Title:\n\n${bookmarkResponse.title}`,
                                                            'type': 'success'
                                                       }
                                                  }, (response) => {
                                                       //window.console.log(response.gotAlert);
                                                  });
                                             });
                                        });
                                   });
                              } else {
                                   //title = bookmarkResponse;
                                   var items = {
                                        id: 1,
                                        title: JSON.stringify(bookmarkResponse),
                                        url: tabs[0].url
                                   }
                                   var database = JSON.stringify([items]);
                                   console.log(database);
                                   _c.cryptog.encryptItem(database).then(function(etext) {
                                        //_c.backgroundProcs.eItem(database).then(function(etext) {
                                        o.opgpbms.db = btoa(etext);
                                        _c.storage.local.set(o, function() {
                                             _c.tabs.sendMessage(tabs[0].id, {
                                                  sendAlert: {
                                                       'header': 'Bookmark Added',
                                                       'text': `Title:\n\n${bookmarkResponse.title}`,
                                                       'type': 'success'
                                                  }
                                             }, (response) => {
                                                  //window.console.log(response.gotAlert);
                                             });
                                        });
                                   });
                              }
                         }
                    })
                    //console.log(tabs[0].url);
               });
          }
     }
     _d.addEventListener('DOMContentLoaded', () => {
          ///////////////////////////
          ///// Document loaded /////
          ///// functions       /////
          ///////////////////////////
          openpgp.config.aead_protect = true;
     });
})(window, document, chrome);


var MENUBOOKMARKIT = null,
     MENUOPENBOOKMARKS = null,
     MENUSEPERATOR = null,
     MENULOGIN = null;
MENULOGOUT = null;
var createContextMenus = function() {
     chrome.storage.local.get('opgpbms', function(o) {
          if(o.opgpbms.LoginTime) {
               if(MENULOGIN) {
                    chrome.contextMenus.remove(MENULOGIN);
                    MENULOGIN = null;
               }
               if(MENUBOOKMARKIT) {
                    chrome.contextMenus.update(MENUBOOKMARKIT, {
                         title: 'Bookmark it!',
                         contexts: ["page"],
                         onclick: chrome.backgroundProcs.promptuser
                    })
               } else {
                    MENUBOOKMARKIT = chrome.contextMenus.create({
                         title: 'Bookmark it!',
                         contexts: ["page"],
                         id: "bookmarkit",
                         onclick: chrome.backgroundProcs.promptuser
                    });
               }

               if(!!MENUOPENBOOKMARKS) {
                    chrome.contextMenus.update(
                         MENUOPENBOOKMARKS, {
                              title: 'View bookmarks',
                              contexts: ["page"],
                              onclick: function() {
                                   chrome.tabs.create({
                                        url: "/html/bookmarks.html"
                                   });
                              }
                         })
               } else {
                    MENUOPENBOOKMARKS = chrome.contextMenus.create({
                         title: 'View bookmarks',
                         contexts: ["page"],
                         id: "viewbookmarks",
                         onclick: function() {
                              chrome.tabs.create({
                                   url: "/html/bookmarks.html"
                              });
                         }
                    });
               }

               if(!!MENUSEPERATOR) {
                    chrome.contextMenus.update(
                         MENUSEPERATOR, {
                              type: "separator"
                         })
               } else {
                    MENUSEPERATOR = chrome.contextMenus.create({
                         type: "separator",
                         id: 'menuseperator'
                    });
               }
               if(!!MENULOGOUT) {
                    chrome.contextMenus.update(
                         MENULOGOUT, {
                              title: "Logout",
                              contexts: ["page"],
                              onclick: chrome.backgroundProcs.clearOBSessionWReload
                         })
               } else {
                    MENULOGOUT = chrome.contextMenus.create({
                         title: "Logout",
                         contexts: ["page"],
                         id: "logout",
                         onclick: chrome.backgroundProcs.clearOBSessionWReload
                    });
               }
          } else {
               if(!!MENULOGIN) {
                    chrome.contextMenus.update(
                         MENULOGIN, {
                              title: "Login",
                              contexts: ["page"],
                              onclick: chrome.backgroundProcs.login
                         })
               } else {
                    MENULOGIN = chrome.contextMenus.create({
                         title: "Login",
                         contexts: ["page"],
                         id: "login",
                         onclick: chrome.backgroundProcs.login
                    });
               }
          }
     });
}
/*
chrome.storage.local.get('opgpbms', function(o) {
	if(o.opgpbms.LoginTime) {

    } else {

    }
});
*/
chrome.tabs.onCreated.addListener((e) => {
     chrome.storage.local.get('bookmarks', (item) => {
          if(item.bookmarks) {
               if(chrome.extension.inIncognitoContext) {
                    createContextMenus();
               }
               if(!chrome.extension.inIncognitoContext) {
                    createContextMenus();
               }
          }
     });
});

chrome.runtime.onInstalled.addListener((details) => {
     if(details.reason == "install") {
          chrome.storage.local.set({
               bookmarks: {
                    "bookmarks": []
               }
          });

          if(chrome.extension.inIncognitoContext) {
               createContextMenus();
          }
          if(!chrome.extension.inIncognitoContext) {
               createContextMenus();
          }

          chrome.tabs.create({
               url: "/html/bookmarks.html"
          });
     }
});


//chrome.contextMenus.onClicked.addListener(chrome.backgroundProcs.promptuser);
chrome.runtime.onStartup.addListener(chrome.backgroundProcs.clearOBSession);
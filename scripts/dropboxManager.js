dcurrentfile = "Running from dropboxManager.js";

((global) => {
	var dropboxManager = {
		version: "1.00",
		access_token: null,
		authUrl: null,
		state: {
			loggedIn: false
		},
		logDebug: (info, error = false) => {
			if(!console.logDebug) {
				var enableDebug = true;
				if(enableDebug) {
					if(error) {
						console.error(info);
					} else {
						console.log(info);
					}
				}
			}
		},
		clients: {
			type: {
				dropbox: {
					name: 'dropbox',
					id: '6238p72v5mzxcr5',
					isAccessTypeToken: false
				},
				paper: {
					name: 'paper',
					id: '9q4p4ph0f93xvcd',
					isAccessTypeToken: false
				}
			}
		},
		stringToBytes: (str) => {
			var bytes = [];
			for(var i = 0; i < str.length; ++i) {
				bytes.push(str.charCodeAt(i));
			}
			return bytes;
		},
		parseQueryString: function(str) {
			console.logDebug(dcurrentfile + ': parseQueryString');
			var ret = Object.create(null);

			if(typeof str !== 'string') {
				return ret;
			}
			str = str.trim().replace(/^(\?|#|&)/, '');

			if(!str) {
				return ret;
			}
			str.split('&').forEach(function(param) {
				var parts = param.replace(/\+/g, ' ').split('=');
				// Firefox (pre 40) decodes `%3D` to `=`
				// https://github.com/sindresorhus/query-string/pull/37
				var key = parts.shift();
				var val = parts.length > 0 ? parts.join('=') : undefined;

				key = decodeURIComponent(key);

				// missing `=` should be `null`:
				// http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
				val = val === undefined ? null : decodeURIComponent(val);

				if(ret[key] === undefined) {
					ret[key] = val;
				} else if(Array.isArray(ret[key])) {
					ret[key].push(val);
				} else {
					ret[key] = [ret[key], val];
				}
			});

			return ret;
		},
		getAuthUrl: function(client) {
			console.logDebug(dcurrentfile + ': getAuthUrl');
			return new Promise((resolve, reject) => {
				if(client == null) {
					console.logDebug(dcurrentfile, true);
					console.logDebugg('Error in getAuthUrl: You must specify a client id.', true);
					reject(false);
				}
				var authUrl;
				var accessTypeToken = client.name;
				var clientId = client.id;
				var randomState = Base64.encode(global.crypto.getRandomValues(new Uint8Array(16)));
				authUrl = 'https://www.dropbox.com/oauth2/authorize?response_type=token&client_id=' +
					clientId +
					'&state=' + encodeURIComponent(randomState) +
					'&redirect_uri=' + encodeURIComponent(chrome.identity.getRedirectURL(accessTypeToken)) +
					'&force_reapprove=false' +
					'&force_reauthentication=true';
				if(authUrl !== null) {
					this.authUrl = authUrl;
					var authParams = {
						authUrl: authUrl,
						randomState: randomState
					}
					//console.log({authParams:authParams});
					resolve(authParams);
				} else {
					console.logDebug(dcurrentfile, true);
					console.logDebugg('Error in getAuthUrl: authUrl is null.', true);
					reject(false);
				}
			});
		},
		output: {},
		interactiveLogin: function(client) {
			console.logDebug(dcurrentfile + ': interactiveLogin');
			var drpm = dropboxManager;//this;
			var promise = new Promise((resolve, reject) => {
				//var randomState = Base64.encode(global.crypto.getRandomValues(new Uint8Array(16)));
				///var authUrl = 'https://www.dropbox.com/oauth2/authorize?response_type=token&client_id=6238p72v5mzxcr5' +
				//	'&state=' + encodeURIComponent(randomState) +
				//	'&redirect_uri=' + encodeURIComponent(chrome.identity.getRedirectURL('dropbox')) +
				//	'&force_reapprove=false' +
				//	'&force_reauthentication=true';
				drpm.getAuthUrl(client).then((authParams) => {
					if(authParams.authUrl === false) {
						console.logDebug(dcurrentfile, true);
						reject('[dropBoxManager.js] Error in interactiveLogin: dbx.getAuthUrl did not return authUrl.');
					} else {
						var dbm = this;
						dbm.output.authUrl = authParams.authUrl;
						settingsManager.launchWebAuthFlow({
							url: authParams.authUrl,
							interactive: true,
							dbm: dbm
						}).then(function(params) {
							//console.log({params: params});
							var redirect_url = params.redirect_url;
							var dbm = params.dbm;
							dbm.output.redirect_url = redirect_url;
							var tokenMatches = /access_token=([^&]+)/.exec(redirect_url);
							var stateMatches = /state=([^&]+)/.exec(redirect_url);
							var uidMatches = /uid=(\d+)/.exec(redirect_url);
							var matchError;
							var failed = false;
							var i = 0;
							if(tokenMatches == null) {
								matchError = 'tokenMatches';
								i++;
								failed = true;
							}
							if(stateMatches == null) {
								matchError = (i == 0) ? 'stateMatches' : matchError + ' and stateMatches';
								i++;
								failed = true;
							}
							if(uidMatches == null) {
								if(matchError != null) {
									if(/^tokenMatches$/.test(matchError)) {
										matchError = 'Error in interactiveLogin: ' + matchError + ' and uidMatches are null.';
									}
									if(/^tokenMatches\sand\sstateMatches/.test(matchError)) {
										matchError = 'Error in interactiveLogin: uidMatechs, ' + matchError + ' are null.';
									}
								} else {
									matchError = 'Error in interactiveLogin: uidMathes is null.';
								}
								i++;
								failed = true;
							}
							if(!failed) {
								var access_token = tokenMatches[1];
								dbm.access_token = access_token;
								dbm.output.access_token = access_token;
								var checkState = decodeURIComponent(decodeURIComponent(stateMatches[1])); //I have no idea why it randomState double-encoded
								var uid = uidMatches[1];
								if(checkState == authParams.randomState) {
									dbm.state.loggedIn = true;
									dbm.output.randomState = authParams.randomState;
									var client = dbm.clients.get();
									dbm.output.client = client;
									//console.log({dbm: dbm});
									settingsManager.saveAccessToken(client.name, access_token).then(function(entries) {
										dbm.output.entries = entries;
										resolve(entries);
									}).catch((error) => {
										console.logDebug(dcurrentfile + ': interactiveLogin', true);
										reject('Error in interactLogin: ' + JSON.stringify(error));
									});
								} else {
									//some sort of error or parsing failure
									reject('Error in interactiveLogin: checkState and randomState do not match.');
									console.logDebug('Error in interactiveLogin: checkState and randomState do not match; redirect_url:' + JSON.stringify(redirect_url), true);
								}
							} else {
								console.logDebug(dcurrentfile + ': interactiveLogin', true);
								console.logDebug('redirect_url: ' + redirect_url, true);
								reject(matchError);

							}
						}).catch((error) => {
							console.logDebug(dcurrentfile + ': interactiveLogin', true);
							reject('Error in interactiveLogin: ' + error);
						});
					}
				});
			});
			return promise;
		},
		getDatabases: function(extension, client) {
			console.logDebug(dcurrentfile + ': getDatabases');
			var data = {
				path: '',
				query: extension,
				start: 0,
				max_results: 100,
				mode: 'filename'
			}
			return this.getToken(client).then(function(accessToken) {
				var req = {
					method: 'POST',
					url: 'https://api.dropbox.com/2/files/search',
					contentType: "application/json",
					dataType: 'json',
					data: JSON.stringify(data),
					headers: {
						'Authorization': `Bearer ${accessToken.dropboxAccessToken}`
					}
				};
				return $.ajax(req);
			}).then(function(response) {
				return response.matches.map(function(fileInfo) {
					return {
						title: fileInfo.metadata.path_display
					};
				});
			});
		},
		getToken: function(client) {
			return new Promise((resolve, reject) => {
				console.logDebug(dcurrentfile + ': getToken');
				var dbm = this;
				console.logDebug(this);
				console.logDebug({
					'getToken client': client
				});
				var tokent;
				settingsManager.getAccessToken(client.name).then(function(stored_token) {
					dropboxManager.state.loggedIn = true;
					resolve(stored_token);
				}).catch((error) => {
					dropboxManager.interactiveLogin(client).then(function(new_token) {
						resolve(new_token);
					});
				});
			});
		},
		listDatabases: function() {
			this.logDebug(dcurrentfile + ': listDatabases');
			return Promise.all([this.getDatabases('.txt')]).then(function(arrayOfArrays) {
				return arrayOfArrays[0];
			}).catch(function(response) {
				if(response.status == 401) {
					//unauthorized, means the token is bad.  retry with new token.
					return interactiveLogin().then(listDatabases);
				}
			});
		},
		getSync: () => {
			console.logDebug("getSync");
			return new Promise((resolve) => {
				chrome.storage.local.get('dropboxSync', (result) => {
					if(result.dropboxSync === undefined) {
						resolve(false);
					} else {
						resolve(result.dropboxSync);
					}
				})
			});
		},
		setSync: (bool) => {
			console.logDebug("setSync");
			return new Promise((resolve) => {
				if(bool) {
					chrome.storage.local.set({
						'dropboxSync': true
					});
					resolve('enable');
				} else {
					chrome.storage.local.set({
						'dropboxSync': false
					});
					resolve('disabled');
				}
			});
		},
		setdbx: (client) => {
			console.logDebug(dcurrentfile + ': setdbx');
			return new Promise(async (resolve) => {
				console.logDebug(dcurrentfile + ': setdbx');
				var token = await dropboxManager.getToken(client);
				var authUrl = await dropboxManager.getAuthUrl(client);
				console.logDebug({
					setdbx: authUrl
				});
				var clientId = dropboxManager.parseQueryString(authUrl.authUrl).client_id;
				console.logDebug('clientId: ' + clientId);
				dropboxManager.dbx = new Dropbox.Dropbox({
					accessToken: token.dropboxAccessToken,
					clientId: clientId
				});
				resolve(true);
			});
		},
		init: function() {
			console.logDebug = dropboxManager.logDebug;
			Object.setPrototypeOf(dropboxManager.clients, {
				get: function() {
					console.logDebug(dcurrentfile + ': get');

					function get() {}
					if(this.type.dropbox.isAccessTypeToken) {
						get.prototype.truetype = this.type.dropbox;
					}
					if(this.type.paper.isAccessTypeToken) {
						get.prototype.truetype = this.type.paper;
					}
					return new get().truetype;
				},
				set: function(item) {
					console.logDebug(dcurrentfile);
					console.logDebug('set: ' + item.name);
					if(item.name === "dropbox") {
						this.type.dropbox.isAccessTypeToken = true;
						this.type.paper.isAccessTypeToken = false;
					}
					if(item.name === "paper") {
						this.type.paper.isAccessTypeToken = true;
						this.type.dropbox.isAccessTypeToken = false;
					}
				}
			});
		}
	}
	global.addEventListener('load', () => {
		var key = 'dropboxAccessToken';
		chrome.storage.local.get([key], (result) => {
			var entries = {};
			entries[key] = result;
			sessionStorage.setItem(key, JSON.stringify(result));
		})
	});
	dropboxManager.init();
	global.dropboxManager = dropboxManager;
})(window);
/*
Object.setPrototypeOf(dropboxManager.accessTypeToken, {
	setTrue: function(item) {
		console.log(item.name);
		if(item.name === "dropbox") {
			this.type.dropbox.isType = true;
		}
		if(item.name === "paper") {
			this.type.paper.isType = true;
		}
	}
});
*/
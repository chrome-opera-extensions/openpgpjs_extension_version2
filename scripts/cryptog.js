(function(_w, _d, _c) {
	_c.cryptog = {
		getDatabase: function() {
			return new Promise(function(resolve) {
				_c.p.storage.local.get('opgpbms').then(function(results) {
					var crt = _c.cryptog.decryptItem(atob(results.opgpbms.db));
					resolve(crt);
				});
			});
		},
		decryptItem: function(item) {
			return new Promise(function(resolve) {
				_c.p.storage.local.get('opgpbms').then(function(results) {
					var o = results;
					var pubkey = o.opgpbms.publicKey;
					pubkey = atob(pubkey);
					var privkey = o.opgpbms.privateKey;
					privkey = atob(privkey);
					openpgp.key.readArmored(privkey).then(function(items) {
						var privKeyObj = items.keys[0];
						var r = o.opgpbms.tgp;
						var c = JSON.parse(r);
						var h = atob((c.endplan.substring(0, c.length)).split('').reverse().join(''));
						var tgp = h.split('').reverse().join('');
						privKeyObj.decrypt(tgp).then((pbool) => {
							if(item !== undefined) {
								openpgp.message.readArmored(item).then(function(encrypted) {
									var message = encrypted
									openpgp.key.readArmored(pubkey).then(function(items) {
										var publicKeys = items.keys
										var options = {
											message: message,
											publicKeys: publicKeys,
											privateKeys: [privKeyObj]
										};
										openpgp.decrypt(options).then(function(plaintext) {
											resolve(plaintext.data);
										}, (error) => {
											resolve(error);
										});
									});
								});
							}
						});
					});
				});
			});
		},
		encryptItem: function(item) {
			//console.log({'item': item});
			return new Promise(function(resolve) {
				_c.p.storage.local.get('opgpbms').then(function(results) {
					var o = results;
					var pgt = o.opgpbms.tgp;
					var rgt = JSON.parse(pgt);
					var cgt = atob((rgt.endplan.substring(0, rgt.length)).split('').reverse().join(''));
					var tgp = cgt.split('').reverse().join('');
					var pubkey = atob(o.opgpbms.publicKey);
					var privkey = atob(o.opgpbms.privateKey);
					openpgp.key.readArmored(pubkey).then(function(pubItems) {
						openpgp.key.readArmored(privkey).then(function(privItems) {
							var privKeyObj = privItems.keys[0];
							privKeyObj.decrypt(tgp).then(function(privBool) {
								if(privBool) {
									var options = {
										message: openpgp.message.fromText(item),
										publicKeys: pubItems.keys,
										privateKeys: [privKeyObj]
									};
									openpgp.encrypt(options).then(function(ciphertext) {
										resolve(ciphertext.data);
									});
								}
							});
						});
					});
				});
			});
		},
	}
})(window, document, chrome);
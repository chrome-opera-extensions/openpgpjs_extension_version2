((_g, _c) => {
	var bookmarkStorage = {
		isOtherBMsSet: false,
		getEBMID: () => {
			///////////////////////////////
			/////   chrome.getEBMID   /////
			///////////////////////////////
			///// Get the ID  of the  ///// 
			///// Encrypted Bookmarks ///// 
			///// folder and setItem  ///// 
			///////////////////////////////
			var continuation = () => {
				return new Promise((accept) => {
					_c.storage.local.get('OTHERBOOKMARKS', (result) => {
						_c.bookmarks.getChildren(result.OTHERBOOKMARKS.id, (results) => {
							var EBMFOLDER = {};
							for(var i = 0; i < results.length; i++) {
								if(results[i].title === "Encrypted Bookmarks") {
									_g.opgpbms.isEncryptedBMsSet = true;
									accept(true);
								}
							}
						});
					});
				});
			}
			return new Promise(function(resolve) {
				if(!_g.opgpbms.isOtherBMsSet) {
					_g.opgpbms.gobkmks().then(() => {
						continuation().then(() => {
							resolve(true);
						}).catch(function(error) {
							_g.opgpbms.logDebug(error);
						});
					}).catch(function(error) {
						_g.opgpbms.logDebug(error);
					});
				} else {
					continuation().then(() => {
						resolve(true);
					}).catch(function(error) {
						_g.opgpbms.logDebug(error);
					});
				}
			});
		}
	}
	_g.bookmarkStorage = bookmarkStorage;
})(window, chrome);
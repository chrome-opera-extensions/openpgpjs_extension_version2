(function(_w, _d, _c) {
    _c.p = {
        storage: {
			local: {
				set: (obj) => {
					return new Promise((resolve) => {
						_c.storage.local.set(obj, () => {
							var error = _c.runtime.lastError;
							if(error) {
								resolve(false);
							} else {
								resolve(true);
							}
						});
					});
				},
				get: (item) => {
					return new Promise((resolve) => {
						_c.storage.local.get(item, (p) => {
							resolve(p);
						});
					});
				},
				remove: (item) => {
					return new Promise((resolve) => {
						chrome.storage.local.remove([item], () => {
							var error = _c.runtime.lastError;
							if(error) {
								resolve(false);
							} else {
								resolve(true);
							}
						});
					});
				}
			}
		}
    }
})(window, document, chrome);
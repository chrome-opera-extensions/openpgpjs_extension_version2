var alertWindow = (jsoninfo) => {
	var cssText = `.swal-button {
		width: 150px !important;
		padding: 7px !important;
		background-color: darkorange !important;
		border-radius: 5px !important;
		border: 2px solid white !important;
		color: white !important;
		cursor: pointer !important;
		font-weight: bold !important;
	}
	.swal-title {
		font-family: Helvetica, Arial, sans-serif !important;
	}
	.swal-button:hover {
		width: 150px !important;
		background-color: white !important;
		border: 2px solid darkorange !important;
		color: darkorange !important;
		cursor: pointer !important;
	}
	.swal-text {
		width: 90% !important;
		text-align: left !important;
		margin: auto !important;
		padding: 10px !important;
		background-color: #EDEDED !important;
		border: 1px solid lightgray !important;
		box-shadow: 1px 1px 15px gray inset !important;
		border-radius: 4px !important;
		font-family: Helvetica, Arial, sans-serif !important;
	}`
	var cssTextNode = document.createTextNode(cssText);
	var style = document.createElement('style');
	style.id = "swalcss";
	style.appendChild(cssTextNode);
	document.head.appendChild(style);
	var alertheader = jsoninfo.header;
	var alertmessage = jsoninfo.text;
	var alerttype = jsoninfo.type;
	swal(alertheader, alertmessage, alerttype);
	document.querySelector('.swal-button.swal-button--confirm').addEventListener('click', function() {
		document.querySelector('#swalcss').remove();
	});
}
var getKeys = (key) => {
	var blob = new Blob([key], {
		type: "text/plain;charset=UTF-8"
	});
	var url = window.URL.createObjectURL(blob);
	var a = document.createElement('a');
	a.href = url;
	a.target = '_blank';
	a.download = "OpenpgpBookmarks_CombinedKey.txt"
	a.id = "CombinedKey";
	document.body.appendChild(a);
	a.click();
}

var getBookmarks = (file) => {
	var blob = new Blob([file], {
		type: "text/plain;charset=UTF-8"
	});
	var url = window.URL.createObjectURL(blob);
	var a = document.createElement('a');
	a.href = url;
	a.target = '_blank';
	a.download = "Openpgpjs_Bookmarks.txt"
	a.id = "Openpgpjs_Bookmarks";
	document.body.appendChild(a);
	a.click();
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if(request.sendAlert) {
		alertWindow(request.sendAlert);
		sendResponse({
			gotAlert: "received"
		});
	}
	if(request.sendkeys) {
		getKeys(request.sendkeys);
		sendResponse({
			gotkeys: "received"
		});
	}
	if(request.sendBookmarks) {
		getBookmarks(request.sendBookmarks);
		sendResponse({
			gotBookmarks: "received"
		});
	}
});
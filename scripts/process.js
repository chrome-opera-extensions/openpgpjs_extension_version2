(() => {
     'use strict';
     chrome.alarms.onAlarm.addListener(function(alarmName) {
          if(window.opgpbms.enableDebug) {
               console.log(window.opgpbms.logPrefix(), "Running Alarm");
               console.log(window.opgpbms.logPrefix(), `Passed Alarm Argument: ${alarmName}`);
          }
          window.opgpbms.checkSession().then((bool) => {
               if(window.opgpbms.enableDebug) {
                    console.log(window.opgpbms.logPrefix(), `Running sessionTimer: running checkSession: returned ${bool}`);
               }
               if(!bool) {
                    window.opgpbms.clearOBSession();
                    window.opgpbms.setLoginTime(true);
                    window.opgpbms.stopSessionTimer("SessionTimer");
                    window.location.reload();
               }
          });
     });
     var opgpbms = {
          geid: document.getElementById.bind(document),
          gecn: document.getElementsByClassName.bind(document),
          getn: document.getElementsByTagName.bind(document),
          gqsa: document.querySelectorAll.bind(document),
          gqs: document.querySelector.bind(document),
          sleep(time) {
               return new Promise((resolve) => setTimeout(resolve, time));
          },
          enableDebug: true,
          logPrefix() {
               return `[OPGPBMS - ${(new Date().toLocaleTimeString("en-US", { hour12: false, hour: "numeric", minute: "numeric", second: "numeric"}))}]:`;
          },
          logDebug(info, error = false) {
               if(!console.logDebug) {
                    console.logDebug = opgpbms.logDebug;
               }
               var enableDebug = true;
               if(enableDebug) {
                    if(error) {
                         console.error(info);
                    } else {
                         console.log(info);
                    }
               }
          },
          showNotification(data, cost) {
               console.log(opgpbms.logPrefix(), data);
               opgpbms.notification = new Notification(data.title, {
                    icon: '../images/orange128.png',
                    body: data.body
               });
               if(cost) {
                    setTimeout(opgpbms.notification.close.bind(opgpbms.notification), cost);
               }
          },
          closeNotification() {
               if(opgpbms.notification) {
                    setTimeout(opgpbms.notification.close.bind(opgpbms.notification), 0);
               }
          },
          $CELS(items) {
               class CELS {
                    constructor(items) {
                         var template = document.createElement('div');
                         if(typeof items === "object") {
                              template.innerHTML = items.html;
                              for(let key in items) {
                                   if((items.hasOwnProperty(key)) && (key !== "html")) {
                                        template.firstChild[key] = items[key];
                                   }
                              }
                         } else {
                              template.innerHTML = items;
                         }
                         this[0] = template.firstChild;
                    }
                    item(i) {
                         return this[i];
                    }
                    appendElement(items) {
                         var template = new CELS(items);
                         this.item(0).appendChild(template[0]);
                    }
                    removeElement(i) {
                         this.item(0).childNodes[i].remove();
                    }
                    appendTo(element) {
                         element.appendChild(this.item(0));
                    }
               }
               return new CELS(items);
          },
          cpd() {
               ///////////////////////////////////
               /////      changePassword     /////
               ///////////////////////////////////
               ///// Change password and     /////
               ///// re-encrypt bookmarks    /////
               ///////////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running cdp");
               }
               //opgpbms.logDebug('Running cpd');
               var p1 = opgpbms.geid('t-changepwd1').value;
               var p2 = opgpbms.geid('t-changepwd2').value;
               //if(p1 === null || p1 === "" || p2 === null || p2 === "") {
               if(!p1 || !p2) {
                    opgpbms.showNotification({
                         title: "Error",
                         body: "Passwords cannot be empty."
                    });
                    //swal('Error', 'Passwords cannot be empty.', 'error');
                    opgpbms.gecn('swal-text')[0].style.textAlign = "center";
               } else if(p1 !== p2) {
                    opgpbms.showNotification({
                         title: "Error",
                         body: "Passwords do not match."
                    });
                    //swal('Error', 'Passwords do not match.', 'error');
                    opgpbms.gecn('swal-text')[0].style.textAlign = "center";
               } else {
                    opgpbms.showNotification({
                         title: "Please Wait...",
                         body: "Building encryption keys."
                    });
                    //swal('Please wait', 'Building encryption keys.', 'warning', {
                    //     buttons: false
                    //});
                    chrome.p.storage.local.get('opgpbms').then((results) => {
                         var pubkey = results.opgpbms.publicKey;
                         pubkey = atob(pubkey);
                         openpgp.key.readArmored(pubkey).then((k) => {
                              var userid = k.keys[0].users[0].userId.userid;
                              userid = userid.match(/<.*>/)[0].replace('<', '').replace('>', '');
                              var options = {
                                   userIds: [{
                                        name: 'Openpgpjs Bookmark user',
                                        email: userid
                                   }],
                                   numBits: 4096,
                                   passphrase: p1
                              };
                              openpgp.generateKey(options).then((keys) => {
                                   var privkey = keys.privateKeyArmored.trim();
                                   var pubkey = keys.publicKeyArmored.trim();

                                   var o = results;
                                   o.opgpbms.publicKey = btoa(pubkey);
                                   o.opgpbms.privateKey = btoa(privkey);
                                   chrome.cryptog.getDatabase().then((db) => {
                                        openpgp.key.readArmored(pubkey).then((pubItems) => {
                                             openpgp.key.readArmored(privkey).then((privItems) => {
                                                  var privKeyObj = privItems.keys[0];
                                                  privKeyObj.decrypt(options.passphrase).then((privBool) => {
                                                       if(privBool) {
                                                            var options = {
                                                                 message: openpgp.message.fromText(db),
                                                                 publicKeys: pubItems.keys,
                                                                 privateKeys: [privKeyObj]
                                                            };
                                                            openpgp.encrypt(options).then((ciphertext) => {
                                                                 o.opgpbms.db = btoa(ciphertext.data);
                                                                 chrome.p.storage.local.set(o).then((bool) => {
                                                                      if(bool) {
                                                                           opgpbms.settgp(p1);
                                                                           opgpbms.closeNotification();
                                                                           opgpbms.showNotification({
                                                                                title: "Congratulations!",
                                                                                body: "Your password has been changed.\nMake sure to export your bookmarks and keys."
                                                                           });
                                                                           //swal('Congratulations!', 'Your password has been changed.\nMake sure to export your bookmarks and keys.', 'success').then(() => {
                                                                           opgpbms.geid('t-changepwd1').value = "";
                                                                           opgpbms.geid('t-changepwd2').value = "";
                                                                           opgpbms.geid('p-changepwd').classList.add('o-hidden');
                                                                           opgpbms.geid('p-changepwd').style.display = 'none';
                                                                           opgpbms.geid('i-checkbox').checked = false;
                                                                           opgpbms.geid('ipass-checkbox').checked = false;
                                                                           opgpbms.geid('iout-checkbox').checked = false;
                                                                           opgpbms.getn('details')[0].removeAttribute('open');
                                                                           //});
                                                                      }
                                                                 });
                                                            });
                                                       }
                                                  });
                                             });
                                        });
                                   });
                              }).catch((error) => {
                                   if(opgpbms.enableDebug) {
                                        console.error(opgpbms.logPrefix(), error);
                                   }
                                   //opgpbms.logDebug(error);
                              });
                         });
                    });
               }
          },
          dBkmk(item) {
               /*async*/
               /////////////////////////////
               /////  decryptBookMark  /////
               /////////////////////////////
               ///// Decrypt bookmarks /////
               /////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running dBkmk");
               }
               //opgpbms.logDebug('Running dBkmk');
               return new Promise((resolve) => {
                    chrome.p.storage.local.get('opgpbms').then((results) => {
                         var o = results;
                         var pubkey = o.opgpbms.publicKey;
                         pubkey = atob(pubkey);
                         var privkey = o.opgpbms.privateKey;
                         privkey = atob(privkey);
                         openpgp.key.readArmored(privkey).then((items) => {
                              var privKeyObj = items.keys[0];
                              var r = o.opgpbms.tgp;
                              var c = JSON.parse(r);
                              var h = atob((c.endplan.substring(0, c.length)).split('').reverse().join(''));
                              var tgp = h.split('').reverse().join('');
                              privKeyObj.decrypt(tgp).then((pbool) => {
                                   if(item !== undefined) {
                                        item = atob(item);
                                        openpgp.message.readArmored(item).then((encrypted) => {
                                             var message = encrypted
                                             openpgp.key.readArmored(pubkey).then((items) => {
                                                  var publicKeys = items.keys
                                                  var options = {
                                                       message: message, //openpgp.message.readArmored(item),
                                                       publicKeys: publicKeys, //openpgp.key.readArmored(pubkey).keys,
                                                       privateKeys: [privKeyObj]
                                                  };
                                                  openpgp.decrypt(options).then((plaintext) => {
                                                       resolve(plaintext.data);
                                                  }, (error) => {
                                                       resolve(error);
                                                  });
                                             })
                                        });
                                   }
                              });
                         });
                    });
               });
          },
          dBkmks(swalhash) {
               ///////////////////////////////////
               /////       decryptBkmks      /////
               ///////////////////////////////////
               ///// Decrypt bookmarks and   /////
               ///// and return an array     /////
               ///////////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running dBkmks");
               }
               return new Promise((resolve) => {
                    chrome.p.storage.local.get('opgpbms').then((results) => {
                         var o = results;
                         if(o.opgpbms.db !== null) {
                              var crt = atob(o.opgpbms.db);
                              chrome.cryptog.decryptItem(crt).then((bmks) => {
                                   var bookmarks = JSON.parse(bmks);
                                   if(bookmarks.length >= bookmarks.length) {
                                        //setTimeout(() => {
                                        opgpbms.closeNotification();
                                        opgpbms.showNotification({
                                             title: swalhash.title,
                                             body: swalhash.body
                                        }, swalhash.cost);
                                        resolve(bookmarks);
                                        /*
                                        swal({
                                             title: swalhash.title,
                                             text: swalhash.text,
                                             icon: 'success',
                                             timer: 2000
                                        }).then(() => {
                                             resolve(bookmarks);
                                        });
                                        */
                                        //}, 5000);
                                   }
                              });
                         }
                    });
               });
          },
          saveHtML(WithPromise = true) {
               if(WithPromise) {
                    return new Promise((resolve) => {
                         chrome.cryptog.encryptItem(document.body.parentElement.outerHTML).then((crypthtml) => {
                              resolve(crypthtml);
                         });
                    });
               } else {
                    chrome.cryptog.encryptItem(document.body.parentElement.outerHTML).then((crypthtml) => {
                         chrome.p.storage.local.get('opgpbms').then((results) => {
                              var o = results;
                              o.opgpbms.bkmkpage = btoa(crypthtml);
                              chrome.p.storage.local.set(o).then((b) => {
                                   if(opgpbms.enableDebug) {
                                        console.log(opgpbms.logPrefix(), "ran saveHTML");
                                   }
                                   //opgpbms.logDebug('ran saveHTML');
                              });
                         });
                    });
               }
          },
          createHTMLElement(element) {
               var template = document.createElement('template');
               var html = element.trim(); // Never return a text node of whitespace as the result
               template.innerHTML = html;
               return template.content.firstChild;
          },
          hashtags: [],
          tagworker(bookmark) {
               bookmark = JSON.parse(bookmark.title);
               //var title = bookmark.title;
               var tags = bookmark.tags;
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), tags);
               }
               tags.forEach((tag) => {
                    if(!opgpbms.hashtags.contains(tag.toLowerCase())) {
                         opgpbms.hashtags.push(tag.toLowerCase());
                    }
                    if(opgpbms.enableDebug) {
                         console.log(opgpbms.logPrefix(), tag.toLowerCase());
                    }
               });
          },
          hashtagWorker(bookmark) {
               return new Promise((resolve) => {
                    //console.log(JSON.parse(bookmark.title));
                    bookmark = JSON.parse(bookmark.title);
                    var title = bookmark.title;
                    var tags = bookmark.tags;
                    var hashtags = document.querySelector('#hashtags');
                    var tagdiv, className;
                    if(tags.length >= 1) {
                         //console.log(tags);
                         tags.forEach(function(tag) {
                              className = tag;
                              if(!/^[A-Za-z]/.test(tag)) {
                                   if(/^[0-9]/.test(tag)) {
                                        className = `_${tag}`;
                                   } else {
                                        className = false;
                                   }
                              }
                              //console.log(tag);
                              //console.log(className);
                              if(className) {
                                   var tester = hashtags.querySelector(`.${tag}`);
                                   if(tester === null) {
                                        tagdiv = opgpbms.createHTMLElement(`<div class="hashtag ${className}">#${tag}</div>`);
                                        hashtags.appendChild(tagdiv);
                                        tagdiv.onclick = function() {
                                             if(this.classList.contains('chosen')) {
                                                  this.classList.remove('chosen');
                                                  var search = document.querySelector('input[aria-controls="bookmarklist"]');
                                                  search.value = "";
                                                  search.dispatchEvent(new Event('focus'));
                                                  search.dispatchEvent(new KeyboardEvent('keyup', {
                                                       'key': 'y'
                                                  }));
                                             } else {
                                                  var search = document.querySelector('input[aria-controls="bookmarklist"]');
                                                  search.value = this.classList[1].replace(/^_/, '');
                                                  document.querySelectorAll('.hashtag').forEach(function(e) {
                                                       if(e.classList.contains('chosen')) {
                                                            e.classList.remove('chosen');
                                                       }
                                                  });
                                                  this.classList.add('chosen');
                                                  search.dispatchEvent(new Event('focus'));
                                                  search.dispatchEvent(new KeyboardEvent('keyup', {
                                                       'key': 'y'
                                                  }));
                                             }
                                        }
                                   }
                              }
                         });
                    }
                    tagdiv = (tagdiv === null) ? false : tagdiv;
                    resolve(tagdiv);
               });
          },
          sortHashtags() {
               /*
               var alphabeticallyOrderedDivs = $('.hashtag').sort(function(a, b) {
               	return String.prototype.localeCompare.call(a.classList[1].toLowerCase(), b.classList[1].toLowerCase());
               });
               $('#hashtags').append(alphabeticallyOrderedDivs);
               */
               var divs = Array.from(document.querySelectorAll('.hashtag')).sort((a, b) => {
                    if(a.classList[1].toLowerCase() < b.classList[1].toLowerCase()) {
                         return -1;
                    } else if(a.classList[1].toLowerCase() > b.classList[1].toLowerCase()) {
                         return 1;
                    } else {
                         return 0;
                    }
               });

               document.querySelectorAll('.hashtag').forEach((e, i) => {
                    e.remove();
               });
               divs.forEach((e) => {
                    document.querySelector('#hashtags').appendChild(e)
               });
          },
          buildPage(passedDB = [], passedHash = []) {
               ///////////////////////////////////
               /////        buildPage       /////
               ///////////////////////////////////
               ///// Get encrypted bookmarks /////
               ///// bookmarks and pass them /////
               ///// to processNode to build /////
               ///// the bookmarks table     /////
               ///////////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running buildPage");
               }
               return new Promise((resolve) => {
                    //var swalhash = {
                    //     title: 'Congratulations',
                    //     text: 'Bookmark table is loaded'
                    //}
                    var swalhash = {
                         "title": "Congratulations",
                         "body": "Bookmark table is loaded",
                         "cost": 10000
                    };
                    //swal('Please wait', 'Decrypting bookmarks...', 'warning', {
                    //     buttons: false
                    //});
                    opgpbms.closeNotification();
                    opgpbms.showNotification({
                         title: "Please Wait...",
                         body: "Decrypting bookmarks..."
                    });
                    console.time('buildPage');
                    console.log(opgpbms.logPrefix(), `PASS DB LENGTH: ${passedDB.length}`);
                    if(passedDB.length === 0) {
                         chrome.p.storage.local.get('opgpbms').then((results) => {
                              var o = results;
                              if(o.opgpbms.db.length !== 0) {
                                   opgpbms.dBkmks(swalhash).then((oHash) => {
                                        var list = opgpbms.gqs("#bookmarklist");
                                        //var th = list.querySelector('thead > tr > th:first-child');
                                        var tbody = list.querySelector('tbody');
                                        oHash.forEach((bookmark) => {
                                             /*
                                             opgpbms.tagworker(bookmark);
                                             opgpbms.hashtagWorker(bookmark).then((b) => {
                                             	if(b) {
                                             		document.querySelector('#hashtags').appendChild(b);
                                             	}
                                             });
                                             */
                                             var bookmarkTitle = JSON.parse(bookmark.title);
                                             var title = bookmarkTitle.title;
                                             var tags = "";
                                             if(bookmarkTitle.tags[0] !== null) {
                                                  bookmarkTitle.tags.forEach((tag) => {
                                                       tags += ` #${tag}`;
                                                       if(!opgpbms.hashtags.contains(tag.toLowerCase())) {
                                                            opgpbms.hashtags.push(tag.toLowerCase());
                                                       }
                                                  });
                                             }
                                             tags = (tags !== "") ? `<span class="ahashtag" style="visibility: hidden;">${tags}</span>` : "<span></span>";
                                             var newnode = `<tr id="_${bookmark.id}"><td>` +
                                                  `<a href="${bookmark.url}" target="_blank">${title}</a>${tags}` +
                                                  `</td><td>` +
                                                  `<a id="dlt" class="_${bookmark.id}" href="#">Delete</a>` +
                                                  `</td></tr>`
                                             tbody.innerHTML += newnode;
                                        });
                                        window.dtable = $('#bookmarklist').DataTable({
                                             lengthMenu: [
                                                  [10, 15, 25, 30, 35, 40, 45, 50, 100, -1],
                                                  [10, 15, 25, 30, 35, 40, 45, 50, 100, 'All']
                                             ],
                                             stripeClasses: ['odd-row', 'even-row'],
                                             fnDrawCallback() {
                                                  $("#bookmarklist tbody tr").on("click", function() {
                                                       $(this).toggleClass("selected");
                                                  });
                                             }
                                        });
                                        /**************************************************/
                                        /* NEW TAG DROPDOWN *******************************/
                                        /**************************************************/
                                        if(document.querySelector("#bookmarklisk_tags")) {
                                             document.querySelector("#bookmarklisk_tags").remove();
                                        }
                                        var dropdown = `<div id="bookmarklisk_tags"></div>`;
                                        dropdown = opgpbms.$CELS(dropdown);
                                        dropdown.appendElement(`<label>Tags: </label>`);
                                        dropdown.appendElement({
                                             html: `<select id="opgpbms_tags" style="text-align-last: center;"><option value>-- Tags --</option></select>`,
                                             onchange: function(evt) {
                                                  var selected = evt.target.options[evt.target.options.selectedIndex].value;
                                                  var search = document.querySelector('#bookmarklist_filter input')
                                                  search.value = selected;
                                                  search.dispatchEvent(new Event('focus'));
                                                  search.dispatchEvent(new KeyboardEvent('keyup', {
                                                       'key': 'y'
                                                  }));
                                             }
                                        });
                                        if(opgpbms.enableDebug) {
                                             console.log(opgpbms.logPrefix(), dropdown.item(0));
                                        }
                                        dropdown.item(0).style.float = "right";
                                        dropdown.item(0).style.marginLeft = "10px";
                                        dropdown.item(0).style.paddingLeft = "5px";
                                        dropdown.item(0).style.borderLeft = "1px solid black";
                                        document.querySelector('#bookmarklist_filter').insertAdjacentElement("beforeBegin", dropdown.item(0));
                                        opgpbms.hashtags.sort();
                                        opgpbms.hashtags.forEach((tag) => {
                                             var option = opgpbms.$CELS(`<option value="${tag.toLowerCase()}">${tag.toLowerCase()}</tag>`);
                                             document.querySelector("#opgpbms_tags").appendChild(option.item(0));
                                        });
                                        /**************************************************/
                                        opgpbms.gqs('#d-bookmarklist').classList.remove('o-hidden');
                                        opgpbms.gqs('#importbms').classList.remove('o-hidden');
                                        opgpbms.saveHtML().then((page) => {
                                             chrome.p.storage.local.get('opgpbms').then(function(results) {
                                                  var o = results;
                                                  o.opgpbms.bkmkpage = btoa(page);
                                                  chrome.p.storage.local.set(o);
                                                  document.querySelector('input[aria-controls="bookmarklist"]').onkeyup = function() {
                                                       if(!!document.querySelector('.hashtag.chosen')) {
                                                            if(this.value !== document.querySelector('.hashtag.chosen').classList[1]) {
                                                                 document.querySelector('.hashtag.chosen').classList.remove('chosen');
                                                            }
                                                       }
                                                  }
                                                  console.timeEnd('buildPage');
                                                  resolve(true);
                                             });
                                        });
                                   }).catch(function(error) {
                                        if(opgpbms.enableDebug) {
                                             console.error(opgpbms.logPrefix(), `Running buildPage error: ${error}`);
                                        }
                                        //opgpbms.logDebug('Running buildPage error: ' + error);
                                        resolve(false);
                                   });
                              } else {
                                   window.dtable = $('#bookmarklist').DataTable({
                                        lengthMenu: [
                                             [10, 15, 25, 30, 35, 40, 45, 50, 100, -1],
                                             [10, 15, 25, 30, 35, 40, 45, 50, 100, 'All']
                                        ],
                                        stripeClasses: ['odd-row', 'even-row'],
                                        fnDrawCallback() {
                                             $("#bookmarklist tbody tr").on("click", function() {
                                                  $(this).toggleClass("selected");
                                             });
                                        }
                                   });
                                   //list.classList.add('compact');
                                   //list.classList.add('cell-border');
                                   //list.classList.add('stripe');
                                   opgpbms.gqs('#d-bookmarklist').classList.remove('o-hidden');
                                   opgpbms.gqs('#importbms').classList.remove('o-hidden');
                                   opgpbms.closeNotification();
                                   opgpbms.showNotification({
                                        title: "Error",
                                        body: "There is no bookmark database"
                                   });
                                   //swal('Error', 'There is no bookmark database.', 'error');
                                   resolve(false);
                              }
                         });
                    } else {
                         window.dtable.clear();
                         window.dtable.destroy();
                         var oHash = passedDB;
                         var list = opgpbms.gqs("#bookmarklist");
                         var tbody = list.querySelector('tbody');
                         //document.querySelectorAll('.hashtag').forEach(e => e.remove());
                         oHash.forEach((bookmark) => {
                              /*
                              opgpbms.hashtagWorker(bookmark).then((b) => {
                                   if(b) {
                                        document.querySelector('#hashtags').appendChild(b);
                                   }
						});
						*/
                              var bookmarkTitle = JSON.parse(bookmark.title);
                              var title = bookmarkTitle.title;
                              var tags = "";
                              if(bookmarkTitle.tags[0] !== null) {
                                   bookmarkTitle.tags.forEach((tag) => {
                                        tags += ` #${tag}`;
                                        if(!opgpbms.hashtags.contains(tag.toLowerCase())) {
                                             opgpbms.hashtags.push(tag.toLowerCase());
                                        }
                                   });
                              }
                              tags = (tags !== "") ? `<span class="ahashtag" style="visibility: hidden;">${tags}</span>` : "<span></span>";
                              var newnode = `<tr id="_${bookmark.id}"><td>` +
                                   `<a href="${bookmark.url}" target="_blank">${title}</a>${tags}` +
                                   `</td><td>` +
                                   `<a id="dlt" class="_${bookmark.id}" href="#">Delete</a>` +
                                   `</td></tr>`
                              tbody.innerHTML += newnode;
                         });
                         window.dtable = $('#bookmarklist').DataTable({
                              lengthMenu: [
                                   [10, 15, 25, 30, 35, 40, 45, 50, 100, -1],
                                   [10, 15, 25, 30, 35, 40, 45, 50, 100, 'All']
                              ],
                              stripeClasses: ['odd-row', 'even-row'],
                              fnDrawCallback() {
                                   $("#bookmarklist tbody tr").on("click", function() {
                                        $(this).toggleClass("selected");
                                   });
                              }
                         });
                         /**************************************************/
                         /* NEW TAG DROPDOWN *******************************/
                         /**************************************************/
                         if(document.querySelector("#bookmarklisk_tags")) {
                              document.querySelector("#bookmarklisk_tags").remove();
                         }
                         var dropdown = `<div id="bookmarklisk_tags"></div>`;
                         dropdown = opgpbms.$CELS(dropdown);
                         dropdown.appendElement(`<label>Tags: </label>`);
                         dropdown.appendElement({
                              html: `<select id="opgpbms_tags" style="text-align-last: center;"><option value>-- Tags --</option></select>`,
                              onchange: function(evt) {
                                   var selected = evt.target.options[evt.target.options.selectedIndex].value;
                                   var search = document.querySelector('#bookmarklist_filter input')
                                   search.value = selected;
                                   search.dispatchEvent(new Event('focus'));
                                   search.dispatchEvent(new KeyboardEvent('keyup', {
                                        'key': 'y'
                                   }));
                              }
                         });
                         if(opgpbms.enableDebug) {
                              console.log(opgpbms.logPrefix(), dropdown.item(0));
                         }
                         dropdown.item(0).style.float = "right";
                         dropdown.item(0).style.marginLeft = "10px";
                         dropdown.item(0).style.paddingLeft = "5px";
                         dropdown.item(0).style.borderLeft = "1px solid black";
                         document.querySelector('#bookmarklist_filter').insertAdjacentElement("beforeBegin", dropdown.item(0));
                         opgpbms.hashtags.sort();
                         opgpbms.hashtags.forEach((tag) => {
                              var option = opgpbms.$CELS(`<option value="${tag.toLowerCase()}">${tag.toLowerCase()}</tag>`);
                              document.querySelector("#opgpbms_tags").appendChild(option.item(0));
                         });
                         /**************************************************/
                         opgpbms.gqs('#d-bookmarklist').classList.remove('o-hidden');
                         opgpbms.gqs('#importbms').classList.remove('o-hidden');
                         opgpbms.saveHtML().then((page) => {
                              chrome.p.storage.local.get('opgpbms').then((results) => {
                                   var o = results;
                                   o.opgpbms.bkmkpage = btoa(page);
                                   chrome.p.storage.local.set(o);
                                   document.querySelector('input[aria-controls="bookmarklist"]').onkeyup = function() {
                                        if(!!document.querySelector('.hashtag.chosen')) {
                                             if(this.value !== document.querySelector('.hashtag.chosen').classList[1]) {
                                                  document.querySelector('.hashtag.chosen').classList.remove('chosen');
                                             }
                                        }
                                   }
                                   console.timeEnd('buildPage');
                                   opgpbms.closeNotification();
                                   opgpbms.showNotification(passedHash[0]);
                                   //swal(passedHash[0]).then(() => {
                                   resolve(true);
                                   //});
                              });
                         });
                    }
               });
          },
          startSessionTimer() {
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running startSessionTimer");
               }
               //opgpbms.logDebug('Running startSessionTimer');
               chrome.alarms.create('SessionTimer', {
                    /*delayInMinutes: 0,*/
                    periodInMinutes: 1
               });
          },
          stopSessionTimer(alarmName) {
               chrome.alarms.clear(alarmName, (result) => {
                    if(opgpbms.enableDebug) {
                         console.log(opgpbms.logPrefix(), `Clearing alarm result: ${result}`);
                    }
                    //opgpbms.logDebug('Clearing alarm result: ' + result);
               });
          },
          setLoginTime(remove = false) {
               var date = new Date();
               date = date.toString();
               if(remove) {
                    chrome.p.storage.local.get('opgpbms').then((results) => {
                         var o = results
                         o.opgpbms.LoginTime = null;
                         chrome.p.storage.local.set(o).then((bool) => {
                              if(bool) {
                                   if(opgpbms.enableDebug) {
                                        console.log(opgpbms.logPrefix(), "Running setLoginTime: LocalTime deleted!");
                                   }
                                   //opgpbms.logDebug('Running setLoginTime: LocalTime deleted!');
                              } else {
                                   var error = chrome.runtime.lastError.message;
                                   if(opgpbms.enableDebug) {
                                        console.error(opgpbms.logPrefix(), `Running setLoginTime: LocalTime deletion error: ${error}`);
                                   }
                                   //opgpbms.logDebug('Running setLoginTime: LocalTime deletion error: ' + error);
                              }
                         });
                    });
               } else {
                    chrome.p.storage.local.get('opgpbms').then((results) => {
                         if(results.opgpbms)
                              var o = results;
                         o.opgpbms.LoginTime = date;
                         chrome.p.storage.local.set(o).then((bool) => {
                              if(bool) {
                                   if(opgpbms.enableDebug) {
                                        console.log(opgpbms.logPrefix(), `Running setLoginTime: ${date}`);
                                   }
                                   //opgpbms.logDebug('Running setLoginTime: ' + date);
                              } else {
                                   var error = chrome.runtime.lastError.message;
                                   if(opgpbms.enableDebug) {
                                        console.error(opgpbms.logPrefix(), `Running setLoginTime: error: ${error}`);
                                   }
                                   //opgpbms.logDebug('Running setLoginTime: error: ' + error);
                              }
                         });
                    });
               }
          },
          getLoginTime() {
               return new Promise((resolve) => {
                    chrome.p.storage.local.get('opgpbms').then((results) => {
                         resolve(results.opgpbms.LoginTime);
                    });
               });
          },
          checkSession() {
               return new Promise((resolve) => {
                    opgpbms.getLoginTime().then((LoginTime) => {
                         if(LoginTime !== null) {
                              var loginTime = new Date(LoginTime);
                              var now = new Date();
                              var diff = +now - +loginTime;
                              var TotalMilliseconds = diff;
                              var TotalSeconds = Math.round(TotalMilliseconds / 1000);
                              var TotalMinutes, TotalTime;
                              if(isNaN(TotalSeconds)) {
                                   TotalTime = NaN;
                                   if(opgpbms.enableDebug) {
                                        console.log(opgpbms.logPrefix(), "checkSession: session is closed");
                                   }
                                   //opgpbms.logDebug('checkSession: session is closed');
                              } else if((TotalSeconds / 60) >= 1) {
                                   TotalMinutes = Math.round(TotalSeconds / 60);
                                   TotalTime = TotalMinutes + ' minutes';
                              } else {
                                   TotalMinutes = parseInt("");
                                   TotalTime = TotalSeconds + ' seconds';
                              }
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), `checkSession: Time logged in = ${TotalTime}`);
                              }
                              //opgpbms.logDebug('checkSession: Time logged in = ' + TotalTime);
                              if(diff < 600000) {
                                   resolve(true);
                              } else {
                                   resolve(false);
                              }
                         } else {
                              resolve(false);
                         }
                    });
               });
          },
          OnClickResetSessionTimer() {
               function setSessionBack() {
                    opgpbms.setLoginTime();
               }
               document.body.removeEventListener("click", setSessionBack);
               document.body.addEventListener("click", setSessionBack);
          },
          checkIfLoggedIn() {
               opgpbms.checkSession().then((bool) => {
                    if(bool) {

                    }
               });
          },
          clearOBSession() {
               chrome.p.storage.local.get('opgpbms').then((results) => {
                    var o = results;
                    o.opgpbms.LoginTime = null;
                    o.opgpbms.bkmkpage = null;
                    o.opgpbms.tgp = null;
                    //o.opgpbms.db = [];
                    chrome.p.storage.local.set(o).then((bool) => {
                         if(bool) {
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), "Session cleared");
                              }
                              //opgpbms.logDebug('Session cleared');
                         } else {
                              var error = chrome.runtime.lastError.message;
                              if(opgpbms.enableDebug) {
                                   console.error(opgpbms.logPrefix(), `clearOBSession error: ${error}`);
                              }
                              //opgpbms.logDebug('clearOBSession error: ' + error);
                         }
                    })
               });
          },
          setLoginPage() {
               ///////////////////////////
               /////  setLoginPage   /////
               ///////////////////////////
               ///// Check if first  /////
               ///// time login and  /////
               ///// enable proper   /////
               ///// elements        /////
               ///////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running setLoginPage");
               }
               //opgpbms.logDebug('Running setLoginPage');
               chrome.p.storage.local.get('opgpbms').then((results) => {
                    var o = results;
                    if(!!o.opgpbms.publicKey) {
                         if(o.opgpbms.publicKey !== null) {
                              opgpbms.checkSession().then(function(bool) {
                                   if(bool) {
                                        if(opgpbms.enableDebug) {
                                             console.log(opgpbms.logPrefix(), "Running setLoginPage: login session is open");
                                        }
                                        //opgpbms.logDebug('Running setLoginPage: login session is open');
                                        opgpbms.gqs('#create-password').classList.add('o-hidden');
                                        opgpbms.gqs('#submit').classList.add('o-hidden');
                                        if(opgpbms.enableDebug) {
                                             console.log(opgpbms.logPrefix(), "Running setLoginPage: buildPage");
                                        }
                                        //opgpbms.logDebug('Running setLoginPage: buildPage');
                                        opgpbms.buildPage().then((bool) => {
                                             if(bool) {
                                                  //opgpbms.sortHashtags();
                                                  if(opgpbms.enableDebug) {
                                                       console.log(opgpbms.logPrefix(), "Running setLoginPage: startSessionTimer");
                                                  }
                                                  //opgpbms.logDebug('Running setLoginPage: startSessionTimer');
                                                  opgpbms.startSessionTimer();
                                                  opgpbms.setLoginTime();
                                                  opgpbms.OnClickResetSessionTimer();
                                             }
                                        });
                                   } else {
                                        if(opgpbms.enableDebug) {
                                             console.log(opgpbms.logPrefix(), "Running setLoginPage: login session is closed");
                                        }
                                        //opgpbms.logDebug('Running setLoginPage: login session is closed');
                                        opgpbms.gqs('#create-password').classList.add('o-hidden');
                                        opgpbms.gqs('#submit').classList.remove('o-hidden');
                                        opgpbms.geid("login-account").addEventListener("click", opgpbms.loginToAcct);
                                        //chrome.browserAction.setIcon({
                                        //	path: '../images/locked128.png'
                                        //});
                                   }
                              });
                         }
                    } else {
                         opgpbms.gqs('#submit').classList.add('o-hidden');
                         opgpbms.gqs('#create-password').classList.remove('o-hidden');
                         opgpbms.geid("setup-account").addEventListener("click", opgpbms.generateKeyPair);
                    }
               });
          },
          generateKeyPair() {
               /////////////////////////////
               /////  generateKeyPair  /////
               /////////////////////////////
               ///// Generate          /////
               ///// Encryption Keys   /////
               /////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running generateKeyPair");
               }
               //opgpbms.logDebug('Running generateKeyPair');
               var newEmail = opgpbms.geid("new-email").value;
               var newPassword = opgpbms.geid("new-password").value;
               var newPasswordRepeat = opgpbms.geid("new-password-repeat").value;

               /* Verify private key encryption password. */
               if(newPassword !== "" && newEmail !== "") {
                    if(newPassword === newPasswordRepeat) {
                         opgpbms.geid("setup-account").innerHTML = "Generating keys";
                         opgpbms.geid("setup-account").disabled = true;

                         /* Prepare to generate keypair. */
                         var options = {
                              userIds: [{
                                   name: 'Openpgpjs Bookmark user',
                                   email: newEmail
                              }],
                              numBits: 4096,
                              passphrase: newPassword
                         };

                         /* Generate keypair, store, and reload the page. */
                         openpgp.generateKey(options).then((key) => {
                              var privkey = key.privateKeyArmored.trim();
                              var pubkey = key.publicKeyArmored.trim();
                              chrome.p.storage.local.get('opgpbms').then(function(results) {
                                   var o = results;
                                   o.opgpbms.privateKey = btoa(privkey);
                                   o.opgpbms.publicKey = btoa(pubkey);
                                   chrome.p.storage.local.set(o).then(function(bool) {
                                        if(bool) {
                                             location.reload();
                                        }
                                   })
                              });
                         });
                    } else {
                         opgpbms.showNotification({
                              title: "Password Error",
                              body: "Passwords do not match."
                         });
                         //swal('Password Error', 'Passwords do not match.', 'error');
                         opgpbms.gecn('swal-text')[0].style.textAlign = "center";
                    }
               } else {
                    opgpbms.showNotification({
                         title: "Password Error",
                         body: "Password cannot be null"
                    });
                    //swal('Password Error', 'Password cannot be null', 'error');
                    opgpbms.gecn('swal-text')[0].style.textAlign = "center";
               }
          },
          settgp(tgp) {
               /////////////////////////////
               /////  setTheGamePlan   /////
               /////////////////////////////
               ///// Set The Game Plan /////
               /////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running settgp");
               }
               //opgpbms.logDebug('Running settgp');
               if(tgp == '') return;
               var y = `Mm4cQ@c*ubXnu2pECads2CJ%Z!UH0Ba8KE9P2EgU@mmS4Hxmhv!WBVtnkBDp0NkWnzyEJm*A%A$Etkk$fUHY7Th?XDN57J*5*f*`;
               y = btoa(y);
               var preplan = btoa(tgp.split('').reverse().join('')).split('').reverse().join('');
               var midplan = preplan + y;
               var rev = {
                    "length": preplan.length,
                    "endplan": midplan
               }
               rev = JSON.stringify(rev);
               chrome.p.storage.local.get('opgpbms').then(function(results) {
                    var o = results;
                    o.opgpbms.tgp = rev;
                    chrome.p.storage.local.set(o).then(function(results2) {
                         if(results2) {
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), "Running tgp: saved");
                              }
                              //opgpbms.logDebug('Running tgp: saved');
                         } else {
                              var error = chrome.runtime.lastError.message;
                              if(opgpbms.enableDebug) {
                                   console.error(opgpbms.logPrefix(), `Running tgp error: ${error}`);
                              }
                              //opgpbms.logDebug('Running tgp error: ' + error);
                         }
                    });
               });
          },
          testPwd() {
               /////////////////////////
               /////    testPwd    /////
               /////////////////////////
               ///// TEST Password /////
               /////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running testPwd");
               }
               //opgpbms.logDebug('Running testPwd');
               return new Promise((resolve) => {
                    opgpbms.checkSession().then(function(bool) {
                         if(bool) {
                              resolve(true);
                         } else {
                              chrome.p.storage.local.get('opgpbms').then(function(results) {
                                   var o = results;
                                   var pgt = o.opgpbms.tgp;
                                   var rgt = JSON.parse(pgt);
                                   var cgt = atob((rgt.endplan.substring(0, rgt.length)).split('').reverse().join(''));
                                   var tgp = cgt.split('').reverse().join('');
                                   var privkey = atob(o.opgpbms.privateKey)
                                   openpgp.key.readArmored(privkey).then((items) => {
                                        var oPrivKey = items.keys[0];
                                        oPrivKey.decrypt(tgp).then(function(verifyagain) {
                                             resolve(true);
                                             opgpbms.setLoginTime();
                                        }).catch(function(err) {
                                             resolve(false);
                                        });
                                   });
                              });
                         }
                    });
               });
          },
          loginToAcct() {
               //////////////////////////////
               /////     loginToAcct    /////
               //////////////////////////////
               ///// Check if session   /////
               ///// is already started /////
               ///// and login          /////
               ///// accordingly        /////
               //////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running loginToAcct");
               }
               //opgpbms.logDebug('Running loginToAcct');
               if(opgpbms.gqs('#login-password').value === "") {
                    opgpbms.showNotification({
                         title: "Password Error",
                         body: "Passwords cannot be empty."
                    });
                    //swal('Error', 'Passwords cannot be empty.', 'error');
                    return;
               }
               opgpbms.gqs('#message').innerText = "";
               chrome.p.storage.local.get('opgpbms').then((results) => {
                    var o = results;
                    if(o.opgpbms.tgp !== null) {
                         opgpbms.testPwd().then(function(isCorrectPass) {
                              if(!isCorrectPass) {
                                   opgpbms.showNotification({
                                        title: "Password Error",
                                        body: "Password is incorrect."
                                   });
                                   //swal('Password Error', 'Password is incorrect', 'error');
                              } else {
                                   if(opgpbms.enableDebug) {
                                        console.log(opgpbms.logPrefix(), 'Running loginToAcct "tgp false": page built');
                                   }
                                   //opgpbms.logDebug('Running loginToAcct "tgp false": page built');
                                   if(opgpbms.enableDebug) {
                                        console.log(opgpbms.logPrefix(), 'Running loginToAcct "tgp true": startSessionTime');
                                   }
                                   //opgpbms.logDebug('Running loginToAcct "tgp true": startSessionTime');
                                   opgpbms.gqs('#create-password').classList.add('o-hidden');
                                   opgpbms.gqs('#submit').classList.add('o-hidden');
                                   opgpbms.buildPage().then((bool) => {
                                        if(bool) {
                                             opgpbms.startSessionTimer();
                                             opgpbms.setLoginTime();
                                             opgpbms.OnClickResetSessionTimer();
                                        }
                                   });
                              }
                         });
                    } else {
                         if(opgpbms.enableDebug) {
                              console.log(opgpbms.logPrefix(), "Running loginToAcct: tgp not stored");
                         }
                         //opgpbms.logDebug('Running loginToAcct: tgp not stored');
                         var p = opgpbms.geid('login-password').value;
                         opgpbms.settgp(p);
                         opgpbms.testPwd().then(function(isCorrectPass) {
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), `Running loginToAcct continuation: ${isCorrectPass}`);
                              }
                              //opgpbms.logDebug('Running loginToAcct continuation: ' + isCorrectPass);
                              if(!isCorrectPass) {
                                   o.opgpbms.tgp = null;
                                   chrome.p.storage.local.set(o).then(function(bool) {
                                        if(bool) {
                                             opgpbms.showNotification({
                                                  title: "Password Error",
                                                  body: "Password is incorrect."
                                             });
                                             //swal('Password Error', 'Password is incorrect', 'error');
                                        } else {
                                             var error = chrome.runtime.lastError.message;
                                             if(opgpbms.enableDebug) {
                                                  console.error(opgpbms.logPrefix(), `Running loginToAcct error: ${error}`);
                                             }
                                             //opgpbms.logDebug('Running loginToAcct error: ' + error);
                                        }
                                   });
                              } else {
                                   if(opgpbms.enableDebug) {
                                        console.log(opgpbms.logPrefix(), 'Running loginToAcct "tgp false": page built');
                                   }
                                   //opgpbms.logDebug('Running loginToAcct "tgp false": page built');
                                   opgpbms.gqs('#submit').classList.add('o-hidden');
                                   opgpbms.gqs('#create-password').classList.add('o-hidden');
                                   opgpbms.buildPage().then((bool) => {
                                        if(bool) {
                                             opgpbms.startSessionTimer();
                                             opgpbms.setLoginTime();
                                             opgpbms.OnClickResetSessionTimer();
                                        }
                                   })
                              }
                         });
                    }
               });
          },
          toggleNewEmailInput() {
               /////////////////////////////////
               /////  toggleNewEmailInput  /////
               /////////////////////////////////
               ///// Input box background  /////
               ///// text                  /////
               /////////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running toggleNewEmailInput");
               }
               //opgpbms.logDebug('Running toggleNewEmailInput');
               var targetvalue = opgpbms.geid('new-email').value;
               opgpbms.geid('new-email').addEventListener('focus', (e) => {
                    if(e.target.value === targetvalue && e.target.value !== "") {
                         e.target.value = "";
                    }
               });
               opgpbms.geid('new-email').addEventListener('blur', (e) => {
                    if(e.target.value !== "") {
                         e.target.classList.remove('empty-input');
                    } else if(e.target.value === "") {
                         e.target.value = targetvalue;
                         e.target.classList.add('empty-input');
                    }
               });
          },
          handleImportKeys(evt) {
               //////////////////////////////
               /////  handleImportKeys  /////
               //////////////////////////////
               ///// Handle uploaded    /////
               ///// Encryption Keys    /////
               //////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running handleImportKeys");
               }
               //opgpbms.logDebug('Running handleImportKeys');
               //try {
               var file = evt.target.files[0];
               if(file === undefined) return;
               if(!/text\/plain/.test(file.type) && file.type !== '') {
                    opgpbms.showNotification({
                         title: "File Error",
                         body: "`Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`"
                    });
                    //swal('File Error', `Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`, 'error')
                    return;
               }
               var reader = new FileReader();
               reader.onload = (event) => {
                    var iFile = event.target.result;
                    if(!(/-----BEGIN\sPGP\sPUBLIC\sKEY\sBLOCK-----/).test(iFile)) {
                         opgpbms.showNotification({
                              title: "File Error",
                              body: "Files are not in the correct format."
                         });
                         //swal('File Error', 'Files are not in the correct format.', 'error');
                         reader.abort();
                         return;
                    }
                    var publines, privlines;
                    var publicKey = /-----BEGIN\sPGP\sPUBLIC\sKEY\sBLOCK-----[\s\S]*-----END\sPGP\sPUBLIC\sKEY\sBLOCK-----/m.exec(iFile);
                    var privateKey = /-----BEGIN\sPGP\sPRIVATE\sKEY\sBLOCK-----[\s\S]*-----END\sPGP\sPRIVATE\sKEY\sBLOCK-----/m.exec(iFile);
                    try {
                         var allPublicLines = publicKey[0].split(/\r\n|\n/);
                         var allPrivateLines = privateKey[0].split(/\r\n|\n/);
                    } catch (err) {
                         opgpbms.showNotification({
                              title: "File Error",
                              body: "Files are not in the correct format."
                         });
                         //swal('File Error', 'Files are not in the correct format.', 'error');
                         reader.abort();
                         return;
                    }

                    allPublicLines.map((line) => {
                         publines = (publines == null) ? `${line}\n` : publines + `${line}\n`;
                    });
                    allPrivateLines.map((line) => {
                         privlines = (privlines == null) ? `${line}\n` : privlines + `${line}\n`;
                    });

                    chrome.p.storage.local.get('opgpbms').then(function(results) {
                         results.opgpbms.publicKey = btoa(publines);
                         results.opgpbms.privateKey = btoa(privlines);
                         chrome.p.storage.local.set(results).then(function(result) {
                              if(result) {
                                   location.reload();
                              } else {
                                   var error = chrome.runtime.lastError.message;
                                   if(opgpbms.enableDebug) {
                                        console.error(opgpbms.logPrefix(), `Running handleImportKeys error: ${error}`);
                                   }
                                   //opgpbms.logDebug('Running handleImportKeys error: ' + error);
                              }
                         });
                    });
               }

               reader.onerror = (evt) => {
                    opgpbms.showNotification({
                         title: "File Error",
                         body: evt.target.error.name
                    });
                    //swal('File error', evt.target.error.name, 'error');
                    reader.abort();
               };
               reader.readAsText(file);
          },
          handleImportBkmks(evt) {
               ////////////////////////////////
               /////   handleImportBkmks  /////
               ////////////////////////////////
               ///// Handles uploaded     /////
               ///// Encryption bookmarks /////
               ////////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running handleImportBkmks");
               }
               //opgpbms.logDebug('Running handleImportBkmks');
               var file = evt.target.files[0];
               if(file === undefined) return;
               if(!/text\/plain/.test(file.type) && file.type !== '') {
                    opgpbms.showNotification({
                         tite: "File Error",
                         body: `Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`
                    });
                    //swal('File Error', `Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`, 'error')
                    return;
               }
               var reader = new FileReader();
               reader.onload = function(event) {
                    var iFile = event.target.result;
                    var bkmklines;
                    if(!(/-----BEGIN\sPGP\sMESSAGE-----/).test(iFile)) {
                         opgpbms.showNotification({
                              tite: "File Error",
                              body: "Files are not in the correct format."
                         });
                         //swal('File Error', 'Files are not in the correct format.', 'error');
                         reader.abort();
                         return;
                    } else {
                         var iFile = event.target.result;
                         var bookmarks = /-----BEGIN\sPGP\sMESSAGE-----[\s\S]*-----END\sPGP\sMESSAGE-----/m.exec(iFile);
                         try {
                              var allBookmarkLines = bookmarks[0].split(/\r\n|\n/);
                         } catch (ex) {
                              opgpbms.showNotification({
                                   tite: "File Error",
                                   body: "Files are not in the correct format."
                              });
                              //swal('File Error', 'Files are not in the correct format.', 'error');
                              reader.abort();
                              return;
                         }
                         allBookmarkLines.map(function(line) {
                              bkmklines = (bkmklines == null) ? `${line}\n` : bkmklines + `${line}\n`;
                         });
                         chrome.p.storage.local.get('opgpbms').then(function(results) {
                              var o = results;
                              o.opgpbms.db = btoa(bkmklines);
                              chrome.p.storage.local.set(o).then(function(bool) {
                                   if(bool) {
                                        location.reload();
                                   }
                              })
                         });
                    }
               };
               reader.onerror = function(evt) {
                    opgpbms.showNotification({
                         title: "File Error",
                         body: evt.target.error.name
                    });
                    //swal('File error', evt.target.error.name, 'error');
                    reader.abort();
               };
               reader.readAsText(file);
          },
          importKey() {
               ///////////////////////
               /////  importKey  /////
               ///////////////////////
               ///// Upload      /////
               ///// Encryption  /////
               ///// Keys        /////
               ///////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running importKey");
               }
               //opgpbms.logDebug('Running importKey');
               opgpbms.geid('importkeys').click();
          },
          importBookmarks: () => {
               /////////////////////////////
               /////  importBookmarks  /////
               /////////////////////////////
               ///// Upload Encryption /////
               ///// bookmrks          /////
               /////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running importBookmarks");
               }
               //opgpbms.logDebug('Running importBookmarks');
               opgpbms.geid('importbmks').click();
          },
          loginEnterKey(e) {
               ///////////////////////////
               /////  loginEnterKey  /////
               ///////////////////////////
               ///// Enter Key Login /////
               ///////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running loginEnterKey");
               }
               //opgpbms.logDebug('Running loginEnterKey');
               if((e.keyCode || e.which) == 13) {
                    opgpbms.geid('login-account').click();
               }
          },
          createdBookmarkWorker(a, b) {
               ///////////////////////////////////
               /////  createdBookmarkWorker  /////
               ///////////////////////////////////
               ///// Adds bookmarks to the   /////
               ///// current session         /////
               ///// background page         /////
               ///////////////////////////////////
               if(opgpbms.enableDebug) {
                    console.log(opgpbms.logPrefix(), "Running createBookmarkWorker");
               }
               //opgpbms.logDebug('Running createBookmarkWorker');
          },
          buildDatabase() {
               return {
                    opgpbms: {
                         privateKey: null,
                         publicKey: null,
                         LoginTime: null,
                         bkmkpage: null,
                         bookmarks: [],
                         tineout: 10,
                         tgp: null,
                         db: [],
                    }
               }
          }
     }
     window.opgpbms = opgpbms;
     Array.prototype.removeItem = function(id) {
          this.splice(id - 1, 1);
     };
     Array.prototype.reorder = function() {
          this.forEach(function(e, i) {
               e.id = i + 1;
          });
     };
     Array.prototype.clone = function() {
          return this.slice(0);
     };
     Array.prototype.contains = function(needle) {
          for(var i in this) {
               if(this[i] === needle) {
                    return true;
               }
          }
          return false;
     }
     document.addEventListener('DOMContentLoaded', function() {
          ///////////////////////////
          ///// Document loaded /////
          ///// functions       /////
          ///////////////////////////
          openpgp.config.aead_protect = true;
          //opgpbms.sortHashtags();
          chrome.p.storage.local.get('opgpbms').then((results) => {
               if(!!results.opgpbms) {
                    if(opgpbms.enableDebug) {
                         console.log(opgpbms.logPrefix(), `DOMContentLoaded opgpgbms store exists: ${true}`);
                    }
                    //opgpbms.logDebug('DOMContentLoaded opgpgbms store exists: ' + true);
                    opgpbms.toggleNewEmailInput();
                    opgpbms.setLoginPage();
               } else {
                    if(opgpbms.enableDebug) {
                         console.log(opgpbms.logPrefix(), `DOMContentLoaded opgpgbms store exists: ${false}`);
                    }
                    //opgpbms.logDebug('DOMContentLoaded opgpgbms store exists: ' + false);
                    chrome.p.storage.local.set(opgpbms.buildDatabase()).then(function(results) {
                         if(results) {
                              opgpbms.toggleNewEmailInput();
                              opgpbms.setLoginPage();
                         } else {
                              var error = chrome.runtime.lastError.message;
                              if(opgpbms.enableDebug) {
                                   console.error(opgpbms.logPrefix(), `DOMContentLoaded error loading db: ${error}`);
                              }
                              //opgpbms.logDebug('DOMContentLoaded error loading db: ' + error);
                         }
                    });
               }
          });

          opgpbms.geid('login-account').addEventListener('click', opgpbms.loginToAcct);
          opgpbms.geid('login-password').addEventListener('keyup', opgpbms.loginEnterKey);
          opgpbms.geid('i-checkbox').addEventListener('change', function() {
               if(opgpbms.geid('i-checkbox').checked) {
                    opgpbms.geid('p-importbms').classList.remove('o-hidden');
                    opgpbms.geid('b-importbms').addEventListener('click', opgpbms.importBookmarks);
               } else {
                    opgpbms.geid('p-importbms').classList.add('o-hidden');
               }
          });
          opgpbms.geid('ipass-checkbox').addEventListener('change', function() {
               if(opgpbms.geid('ipass-checkbox').checked) {
                    opgpbms.geid('p-changepwd').classList.remove('o-hidden');
                    opgpbms.geid('p-changepwd').style.display = 'grid';
                    opgpbms.geid('b-changepwd').addEventListener('click', opgpbms.cpd);
               } else {
                    opgpbms.geid('p-changepwd').classList.add('o-hidden');
                    opgpbms.geid('p-changepwd').style.display = 'none';
               }
          });
          opgpbms.geid('iout-checkbox').addEventListener('change', function() {
               if(opgpbms.geid('iout-checkbox').checked) {
                    opgpbms.geid('p-logout').classList.remove('o-hidden');
                    opgpbms.geid('b-logout').addEventListener('click', function() {
                         opgpbms.clearOBSession();
                         window.location.reload();
                    });
               } else {
                    opgpbms.geid('p-logout').classList.add('o-hidden');
               }
          });
          opgpbms.geid('isession-checkbox').addEventListener('change', function() {
               if(opgpbms.geid('isession-checkbox').checked) {
                    opgpbms.geid('p-session').classList.remove('o-hidden');
                    opgpbms.geid('p-session').style.display = 'block';
               } else {
                    opgpbms.geid('p-session').classList.add('o-hidden');
                    opgpbms.geid('p-session').style.display = 'none';
               }
          });
          opgpbms.getn('summary')[0].addEventListener('click', function() {
               if(opgpbms.getn('details')[0].getAttribute('open') === "") {
                    opgpbms.geid('i-checkbox').checked = false;
                    opgpbms.geid('ipass-checkbox').checked = false;
                    opgpbms.geid('iout-checkbox').checked = false;
                    opgpbms.geid('p-changepwd').classList.add('o-hidden');
                    opgpbms.geid('p-changepwd').style.display = 'none';
                    opgpbms.geid('p-importbms').classList.add('o-hidden');
               }
          });
          opgpbms.geid('d-bookmarklist').addEventListener("click", async (e) => {
               if(e.target.id == "dlt") {
                    opgpbms.showNotification({ "title": "Please Wait...", "body": "Deleting bookmark..." });
                    //swal('Please wait', 'Deleting bookmark...', 'warning', {
                    //    buttons: false
                    //});
                    chrome.p.storage.local.get('opgpbms').then(function(results) {
                         var o = results;
                         //o.opgpbms.bkmkpage = page;
                         var crt = atob(o.opgpbms.db);
                         chrome.cryptog.decryptItem(crt).then(function(db) {
                              //opgpbms.dBkmk(o.opgpbms.db).then(function(db) {
                              var id = /_(.*)/.exec(e.target.classList[0])[1];
                              document.querySelectorAll('.hashtag').forEach(function(e) {
                                   var tag = e.classList[1].replace("_", "");
                                   var text = document.querySelector(`#_${id} span`);
                                   if(text !== null) {
                                        text = text.innerHTML;
                                        text = (/\s/.test(text)) ? text.split(" ") : [text];
                                        var rex = new RegExp(tag, 'gmi');
                                        text.forEach(function(t) {
                                             if(rex.test(t.trim())) {
                                                  document.querySelector(`.${e.classList[1]}`).remove();
                                             }
                                        });
                                   }
                              });
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), `id: ${id}`);
                              }
                              //opgpbms.logDebug('id: ' + id);
                              //var index = Array.from(document.querySelectorAll('td:nth-child(2) a')).indexOf(document.querySelector('._' + id));
                              db = JSON.parse(db);
                              //db = db.splice(index, 1);
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), {
                                        "db_before_slice_and_reorder": db
                                   });
                              }
                              db.removeItem(id);
                              db.reorder();
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), {
                                        "db _after_slice_and_reorder": db
                                   });
                              }
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), `lllllllllleeeeeeeennnnnggggtttttthhhh: ${db.length}`);
                              }
                              //opgpbms.logDebug('lllllllllleeeeeeeennnnnggggtttttthhhh: ' + db.length);
                              if(db.length === 0) {
                                   o.opgpbms.db = [];
                                   chrome.p.storage.local.set(o);
                                   opgpbms.clearOBSession();
                                   dtable.row($('#_' + id)).remove();
                                   opgpbms.geid('_' + id).remove();
                                   dtable.draw();
                                   opgpbms.closeNotification();
                                   opgpbms.showNotification({
                                        "title": "Deleted",
                                        "body": "Bookmark deleted!"
                                   }, 10000);
                                   //swal({
                                   //     title: 'Deleted',
                                   //     text: 'Bookmark deleted!',
                                   //     icon: 'success',
                                   //     timer: 2000
                                   //});
                              } else {
                                   chrome.cryptog.encryptItem(JSON.stringify(db)).then(function(result2) {
                                        o.opgpbms.db = btoa(result2);
                                        chrome.p.storage.local.set(o).then(function(b) {
                                             //var swalhash = [{
                                             //     title: "Deleted",
                                             //     text: "Bookmark deleted!",
                                             //     icon: 'success',
                                             //     timer: 2000
                                             //}];
                                             var swalhash = [{
                                                  "title": "Deleted",
                                                  "body": "Bookmark deleted!",
                                                  "cost": 10000
                                             }];
                                             //var id = /_(.*)/.exec(e.target.classList[0])[1];
                                             //dtable.row($('#_' + id)).remove();
                                             //opgpbms.geid('_' + id).remove();
                                             //dtable.draw();
                                             //opgpbms.saveHtML(false);
                                             opgpbms.hashtags = [];
                                             opgpbms.buildPage(db, swalhash).then((bool) => {
                                                  if(bool) {
                                                       if(opgpbms.enableDebug) {
                                                            console.log(opgpbms.logPrefix(), "Running setLoginPage: startSessionTimer");
                                                       }
                                                       opgpbms.startSessionTimer();
                                                       opgpbms.setLoginTime();
                                                  }
                                             });
                                             //opgpbms.sortHashtags();
                                        });

                                        //syncBookmarks();
                                   });
                              }
                         });
                    });
                    //});
               } else if(e.target.id !== 'dlt' && e.target.tagName === 'A') {
                    //var thislink = e.target;
                    //if(e.target.target === '_blank') {
                    //     if(opgpbms.enableDebug) {
                    //          console.log(opgpbms.logPrefix(), "d-bookmarklist: bookmark opened, restarting session");
                    //     }
                    //opgpbms.logDebug('d-bookmarklist: bookmark opened, restarting session');
                    //     opgpbms.setLoginTime();
                    //}
               }
          });

          opgpbms.geid('b-importkey').addEventListener('click', opgpbms.importKey);
          opgpbms.geid('importkeys').addEventListener('change', opgpbms.handleImportKeys, false);
          opgpbms.geid('importbmks').addEventListener('change', opgpbms.handleImportBkmks, false);
     });
     document.addEventListener('DOMNodeInserted', (evt) => {
          if(evt.target.tagName == 'TR' && evt.target.getAttribute('role') == null) {
               try {
                    var ex = document.getElementById('bookmarklist');
                    if($.fn.DataTable.fnIsDataTable(ex)) {
                         target = evt.target;
                         var bmarktitle = evt.target.getElementsByTagName('td')[0].getElementsByTagName('a')[0].innerText;
                         bmarktitle = `Title:\n\n${bmarktitle}`;
                         opgpbms.showNotification({
                              title: "Bookmark Added",
                              body: bmarktitle
                         }, 10000);
                         //swal({
                         //     title: 'Bookmark Added',
                         //     text: bmarktitle,
                         //     icon: 'success',
                         //     timer: 5000
                         //});
                         dtable.row.add(evt.target).draw();
                    }
               } catch (err) {}
          }
     }, true);

     chrome.storage.onChanged.addListener((changes, namespace) => {
          for(let key in changes) {
               var storageChange = changes[key],
                    o = storageChange.oldValue;
               if(o) {
                    if(storageChange.oldValue.db !== storageChange.newValue.db && storageChange.newValue.db.length > storageChange.oldValue.db.length) {
                         var crt = atob(storageChange.newValue.db);
                         chrome.cryptog.decryptItem(crt).then((results) => {
                              var o = JSON.parse(results),
                                   last = o[o.length - 1],
                                   id = last.id,
                                   du = last.url,
                                   dt = last.title,
                                   tbody = opgpbms.gqs("#bookmarklist > tbody");
                              /*
                              opgpbms.hashtagWorker(last).then(function(b) {
                                   if(b) {
                                        document.querySelector('#hashtags').appendChild(b);
                                   }
						});
						*/
                              var bookmarkTitle = JSON.parse(dt);
                              var title = bookmarkTitle.title;
                              var tags = "";
                              if(bookmarkTitle.tags[0] !== null) {
                                   bookmarkTitle.tags.forEach((tag) => {
                                        tags += ` #${tag}`;
                                        if(!opgpbms.hashtags.contains(tag.toLowerCase())) {
                                             opgpbms.hashtags.push(tag.toLowerCase());
                                        }
                                   });
                              }
                              tags = (tags !== "") ? `<span class="ahashtag" style="visibility: hidden;">${tags}</span>` : "<span></span>";
                              var node = document.createElement('tr');
                              node.id = "_" + id;
                              node.innerHTML = `<td>` +
                                   `<a href="${du}" target="_blank">${title}</a>${tags}` +
                                   `</td>` +
                                   `<td>` +
                                   `<a id="dlt" class="_${id}" href="#">Delete</a>` +
                                   `</td>`;
                              //tbody.innerHTML += node;
                              //opgpbms.sortHashtags();
                              dtable.row.add(node).draw();
                              /**************************************************/
                              /* NEW TAG DROPDOWN *******************************/
                              /**************************************************/
                              if(document.querySelector("#bookmarklisk_tags")) {
                                   document.querySelector("#bookmarklisk_tags").remove();
                              }
                              var dropdown = `<div id="bookmarklisk_tags"></div>`;
                              dropdown = opgpbms.$CELS(dropdown);
                              dropdown.appendElement(`<label>Tags: </label>`);
                              dropdown.appendElement({
                                   html: `<select id="opgpbms_tags" style="text-align-last: center;"><option value>-- Tags --</option></select>`,
                                   onchange: function(evt) {
                                        var selected = evt.target.options[evt.target.options.selectedIndex].value;
                                        var search = document.querySelector('#bookmarklist_filter input')
                                        search.value = selected;
                                        search.dispatchEvent(new Event('focus'));
                                        search.dispatchEvent(new KeyboardEvent('keyup', {
                                             'key': 'y'
                                        }));
                                   }
                              });
                              if(opgpbms.enableDebug) {
                                   console.log(opgpbms.logPrefix(), dropdown.item(0));
                              }
                              dropdown.item(0).style.float = "right";
                              dropdown.item(0).style.marginLeft = "10px";
                              dropdown.item(0).style.paddingLeft = "5px";
                              dropdown.item(0).style.borderLeft = "1px solid black";
                              document.querySelector('#bookmarklist_filter').insertAdjacentElement("beforeBegin", dropdown.item(0));
                              opgpbms.hashtags.sort();
                              opgpbms.hashtags.forEach((tag) => {
                                   var option = opgpbms.$CELS(`<option value="${tag.toLowerCase()}">${tag.toLowerCase()}</tag>`);
                                   document.querySelector("#opgpbms_tags").appendChild(option.item(0));
                              });
                              /**************************************************/
                              var bmarktitle = `Title:\n\n${title}`;
                              opgpbms.showNotification({
                                   title: "Bookmark Added",
                                   body: bmarktitle
                              }, 10000);
                              //swal({
                              //     title: 'Bookmark Added',
                              //     text: bmarktitle,
                              //     icon: 'success',
                              //     timer: 5000
                              //});
                         });
                         //console.log(storageChange.oldValue.db);
                         //console.log(storageChange.newValue.db);
                    }
                    if(storageChange.newValue.tgp === null && storageChange.newValue.LoginTime === null) {
                         window.location.reload();
                    }
               }
               //console.log('Storage key "%s" in namespace "%s" changed. Old value was "%s", new value is "%s".',
               //	key, namespace, storageChange.oldValue, storageChange.newValue);
          }
     });
})();


/*
NodeList.prototype.classesContains = function(needle) {
	for(var i in this) {
		if(this[i].classList.contains(needle)) {
			return true;
        }
    }
	return false;
}
NodeList.prototype.hasClass = function(needle) {
	var container = [];
	for(var i in this) {
		if(this[i].classList) {
            if(this[i].classList.contains(needle)) {
                container.push(this[i]);
            }
        }
    }
	return container;
}
*/
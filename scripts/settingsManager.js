scurrentfile = "Running from settingsManager.js";

((global) => {
var settingsManager = {
		saveAccessToken: function(type, accessToken) {
			console.log(scurrentfile + ': saveAccessToken');
			return new Promise(function(resolve, reject) {
				var entries = {};
				entries[type + 'AccessToken'] = accessToken;
				console.log('saveAccessToken stored entries: ' + JSON.stringify(entries));
				chrome.storage.local.set(entries, function() {
					if(chrome.runtime.lastError) {
						console.error(scurrentfile + ': saveAccessToken');
						var rejectResponse = {
							method: 'saveAccessToken',
							error: chrome.runtime.lastError.message,
							entries: entries
						}
						reject({'Error in saveAccessToken': rejectResponse});
					} else {
						resolve(entries);
					}
				});
			})
		},
		getAccessToken: function(type) {
			console.log(scurrentfile + ': getAccessToken');
			return new Promise(function(resolve, reject) {
				var key = type + 'AccessToken';
				console.log('[settingsManager.js] getAccessToken key : ' + key);
				chrome.storage.local.get([key], function(items) {
					if(chrome.runtime.lastError) {
						reject(new Error(chrome.runtime.lastError.message));
					} else {
						if(items[key]) {
							resolve(items);
						} else {
							console.error(scurrentfile + ': getAccessToken');
							reject('Error in getAccessToken: The key does not exist in chrome storage.');
						}
					}
				});
			});
		},
		launchWebAuthFlow: function(params) {
               console.log(scurrentfile + ': launchWebAuthFlow');
               console.log(scurrentfile + ': ' + JSON.stringify(params));
			return new Promise(function(resolve, reject) {
				chrome.identity.launchWebAuthFlow({url:params.url,interactive: params.interactive}, function(redirect_url) {
					if(chrome.runtime.lastError) {
						console.error(scurrentfile + ': launchWebAuthFlow');
						reject('Error in launchWebAuthFlow: ' + chrome.runtime.lastError.message);
					} else if(redirect_url) {
						resolve({redirect_url: redirect_url, dbm: params.dbm});
					} else {
						console.error(scurrentfile + ': launchWebAuthFlow');
						reject(new Error("Erron in launchWebAuthFlow: Unknown error in oauth2 web flow"));
					}
				});
			});
		},

	}
	global.settingsManager = settingsManager;
})(window);